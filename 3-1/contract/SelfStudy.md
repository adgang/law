# Syllabus

PAPER-I: LAW OF CONTRACT–I

## Unit-I
Definition and essentials of a valid Contract - Definition and essentials of a valid Offer - Definition and essentials  of  valid  Acceptance  -  Communication  of  Offer  and  Acceptance  -  Revocation  of  Offer  and  Acceptance through various modes including electronic medium - Consideration - salient features - Exception to consideration - Doctrine of Privity of Contract - Exceptions to the privity of contract - Standard form of Contract.

## Unit-II
Capacity  of  the  parties  -  Effect  of  Minor's  Agreement  -  Contracts  with  insane  persons  and  persons disqualified by law - Concepts of Free Consent - Coercion - Undue influence - Misrepresentation - Fraud - Mistake -  Lawful  Object  -  Immoral  agreements  and  various  heads  of  public  policy  -  illegal  agreements  -  Uncertain agreements  - Wagering agreements - Contingent contracts - Void and Voidable contracts.

## Unit-III
Discharge of Contracts - By performance - Appropriation of payments - Performance by joint promisors - Discharge  by  Novation  -  Remission  -  Accord  and  Satisfaction  -  Discharge  by  impossibility  of  performance (Doctrine of Frustration) - Discharge by Breach - Anticipatory Breach - Actual breach.

## Unit-IV
Quasi  Contract  -  Necessaries  supplied  to  a  person  who  is  incapable  of  entering  into  a  contract  - Payment by an interested person - Liability to pay for non-gratuitous acts -  Rights of finder of lost goods - Things delivered by mistake or coercion - Quantum merit - Remedies for breach of contract - Kinds of damages - liquidated and unliquidated damages and penalty - Duty to mitigate.

## Unit-V
Specific Relief - Recovering possession of property - Specific performance of the contract - Rectification of instruments  -  Rescission  of  contracts  -  Cancellation  of  instruments  -  Declaratory  Decrees  -  Preventive  Relief  - Injunctions - Generally - Temporary and Perpetual injunctions - Mandatory & Prohibitory injunctions - Injunctions to perform negative agreement.

## Suggested Readings:
1. Anson: Law of Contract, Clarendon Press, Oxford, 1998.
2. Krishnan Nair: Law of Contract, S.Gogia & Co., Hyderabad 1995.
3. G.C.V. Subba Rao: Law of Contract, S.Gogia & Co., Hyderabad 1995.
4. T.S.Venkatesa Iyer: Law of Contract, revised by Dr. Krishnama Chary, S. Gogia & Co.
5. Avtar Singh: Law of Contract, Eastern Book Company, Lucknow, 1998.

# Study Notes

Sun 25 Feb 2018 11:17:35

Act IX of 1872  
Indian Contract Act 1872


-----

### Part A - 6 marks each - 5/8 * 6 = 30
- 2 pages preferably per question

### Part B - 15 marks each - 2/4 * 15 = 30
Answer any 2 - may be 5 pages each

### Part C - 2/4 * 10 = 20
Application problems  
2 - maximum 3 pages


28 pages - 180 min
6 min a page



Part A - one liner then full explanation

Part B - full exposition

Part C - Short Answer, Facts in terms of law, Law related to each action and its consequences,  
