# Syllabus
(as per [OU Website](http://www.osmania.ac.in/lawcollege/syllabi.html))

## PAPER-III: CONSTITUTIONAL LAW-I

## Unit-I
Constitution-Meaning and Significance - Evolution of Modern Constitutions -Classification of Constitutions-Indian  Constitution  - Historical  Perspectives  - Government  of  India  Act,  1919  - Government  of  India  Act,  1935  - Drafting of Indian Constitution - Role of Drafting Committee of the Constituent Assembly

## Unit-II
Nature  and  Salient  Features  of  Indian  Constitution  -  Preamble  to  Indian  Constitution  -  Union  and  its Territories-Citizenship - General Principles relating to Fundamental Rights(Art.13) - Definition of State

## Unit-III
Right to Equality(Art.14-18) – Freedoms and Restrictions under Art.19 - Protection against Ex-post facto law - Guarantee against Double Jeopardy - Privilege against Self-incrimination - Right to Life and Personal Liberty - Right to Education – Protection against Arrest and Preventive Detention

## Unit-IV
Rights  against  Exploitation  - Right  to  Freedom  of  Religion  - Cultural  and  Educational  Rights  - Right  to Constitutional Remedies - Limitations on Fundamental Rights(Art.31-A,B and C)

## Unit-V
Directive  Principles  of  State  Policy  –  Significance  –  Nature  –  Classification  -   Application  and  Judicial Interpretation  -  Relationship  between  Fundamental  Rights  and  Directive  Principles  -  Fundamental  Duties  – Significance - Judicial Interpretation

## Suggested Readings:
1. M.P.Jain, Indian Constitutional Law, Wadhwa & Co, Nagpur
2. V.N.Shukla, Constitution of India, Eastern Book Company, Lucknow
3. Granville Austin, Indian Constitution-Cornerstone of a Nation, OUP, New Delhi
4. H.M.Seervai, Constitutional Law of India(in 3 Volumes), N.M.Tripathi, Bombay
5. G.C.V.Subba Rao, Indian Constitutional Law, S.Gogia & Co., Hyderabad
6. B.Shiva  Rao: Framing  of  India’s  Constitution(in  5  Volumes),  Indian  Institute  of  Public  Administration,  New Delhi
7. J.N.Pandey, Constitutional Law of India, Central Law Agency, Allahabad


# Study Notes

## Thu 08 Mar 2018 01:01:35

Syllabus: Articles 1 -  51 A

PART - 1 : Articles 1 - 4, The Union And Its Territory  
PART - 2 : Articles 5 - 11, Citizenship  
PART - 3 : Articles 12 - 35, Fundamental Rights  
PART - 4 : Articles 36 - 51, Directive Principles of State Policy  
PART - 4A : Article 51A, Fundamental Duties


2260
285

2260(1.072)^x * 0.07 = 285(1.055)^x


2260*0.07 / 285 = 0.985^x

0.555 = 0.985^x

0.7743 = x
-0.2557 = x *  - 0.0065638
----
38.95

2260 (1.1)^x * 0.1 = 285(1.05)^x


1.0476^x = 1.2611


x * 0.0202= 0.1007
4.98 years

After 5 years at 10%
3639bil
363bil


339 bil
22.6bil

27.5%

20%


623bil
54bil

11.54 * 0.07=
0.8


0.0082^x = 0.8

0.8 = (1.05/1.07)^x

0.98 - 0.0082
0.8 - 0.097
