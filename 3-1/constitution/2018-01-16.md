Tue 16 Jan 2018 10:43:27 AM
------

# Constitution

## Article 12

Does airport come under Article 12?
Yes.

RD Shetty Vs IAAI
[RAMANA DAYARAM SHETTY Vs THE INTERNATIONAL AIRPORT AUTHORITY OF INDIA AND ORS.](https://indiankanoon.org/doc/1281050/) on 4 May, 1979
Equivalent citations: 1979 AIR 1628, 1979 SCR (3)1014

1. All investment is by central government
2. Central government ahs authority to appoint Chairman and any member of board.
3. Central Government has power to terminate who was appointed.

Sukhdev Singh & ONGC Vs Bhagat Ram
[SUKHDEV SINGH & ORS Vs BAGATRAM SARDAR SINGH RAGHUVANSHI AND ANR.](https://indiankanoon.org/doc/974148/) on 21 February, 1975
Equivalent citations: AIR 1975 SC 1331, 1975 (30) FLR 283, 1975 LablC 881, (1975) ILLJ 399 SC, (1975) 1 SCC 421, 1975 3 SCR 619




ONGC, LIC, IFCI all come under Article 12. They can be called state. They are established under a statute.


### What if Union government has minimal investment

Where little government participation amounts to limited control, which is purely regulatory.

Ex: ICRISAT is not state.

Whether BCCI is state?
No.

Zee Telefilms Ltd Vs Union of India
[Zee Telefilms Ltd. & Anr vs Union Of India & Ors](https://indiankanoon.org/doc/404603/) on 2 February, 2005


CSIR - Council of Scientific and Industrial Research

CSIR is state under Article 12.  
SAIL is state under Article 12.  

Central Inland Water Transport Corporation Vs Brojo Nath Ganguly  
[Central Inland Water Transport Corporation vs Brojo Nath Ganguly & Anr](https://indiankanoon.org/doc/477313/) on 6 April, 1986
Equivalent citations: 1986 AIR 1571, 1986 SCR (2) 278


Article 14 for principles of natural justice.



Guidelines were laid down in the following cases:

1. Ajay Hasia Etc vs Khalid Mujib Sehravardi

    [Ajay Hasia Etc vs Khalid Mujib Sehravardi & Ors. Etc](https://indiankanoon.org/doc/1186368/) on 13 November, 1980
    Equivalent citations: 1981 AIR 487, 1981 SCR (2) 79
2. Chandramohan Khanna Vs NCERT

    [Chander Mohan Khanna vs National Council Of Educational Research And Training And Ors](https://indiankanoon.org/doc/700858/) on 17 September, 1991

    Equivalent citations: 1992 AIR 76, 1991 SCR Supl. (1) 165


The guidelines for determining wheter a corporation can be called state under Article 12 are:

1. Entire share capital of the corporation is held by government.
2. Financial assistance to corporation - to meet entire expenditure and showing governmental characteristics.
3. Wherein corporation enjoys monopoly status
4. wherein state exercises 'deep and  pervasive' control.(Existence of	 'deep and  pervasive  State
control')
5. Functions are of public importance and closely related to governmental functions.
6. if a department of government is transferred to a corporation.


The test is not as to how the juristic person is created but why it has been brought into existence.


----------------
