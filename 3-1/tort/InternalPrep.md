


1. Salus populi suprema lex esto - welfare of people is supreme.
2. Absolute Liability is laid down in MCMehta Vs UOI(1986) aka Oleum Leak Case involving Shriram Foods and Fertilisers
3. When 2 persons independently to produce single damage they are called independent tortfeasors
4. Wrongful act done intentionally without just cause or excuse is called malice by law.
5. Tort is a civil right.
6. mens rea means guilty mind.
7. Tortuous liability arises from the breach of a duty primarily fixed by the law. This duty is towards persons generally and its breach is redressable by action for unliquidated damages - Windfield
8. Under law of torts, duties are imposed by law.
9. When harm is done caused intentionally to prevent greater essential good, defence of necessity  is available as defence.
10. Ex turpi causa non oritur actio means dishonourable cause of action.
11. If A walks across B's land without permission, A committed trespass
12. Vicarious Liability means the assignment of liability to someone who did not directly cause the harm.
13. An accident due to natural causes without human interference is Act of God.
14. Gloucester Grammar School explains Damnum sine injuria
15. In tort, mistake is general defence.
16. Rylands Vs Fletcher laid down strict liability rule
17. All persons who cousel, aid or direct or join in the committed wrongful act are called joint tortfeasors.
18. Is a minor liable under law of tort? Yes
19. When plaintiff consented to suffer some harm it is known as volenti non fit injuria.
20. Injuria Sine Damnum means injury without damage.
21. Negligence is laid down in Donoghue v Stevenson case.



## Paper Discussion

Latent Defect
