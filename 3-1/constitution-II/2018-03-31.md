Wed 28 Mar 2018 10:28:07 AM
-------


# Constitution II


## State Emergency

President's satisfaction based on report of Governor of the state with regard to breakdown of constitutional machinery or failure of state to work according to constitution.


Article 356 is a valve in case of disruption of political machinery in the state.


1. President will assume all powers of the state.
2. He can even take over responsibilities of High Court.
2. Union(Parliament and Executive) will assume all legislative and executive functions of the state.
3. State Legislature and Executive will be put under suspended animation.
4. President can pass an ordinance. He also has power to release funds from Consolidated Fund of India to State's consolidated fund or release funds from State's consolidated fund. But this has to be ratified by Parliament.
5. Law making power is given to Governor.


Duration:
1. It can be for two months at a stretch.
2. Before 30 days of first sitting of
3. 3 years maximum state emergency - only exception being Punjab emergency during Blue Star.


This Article has been imposed almost 100 times.

SR Bommai Vs UOI

How and when can emergency be proclaimed?

1. s
2. when Governor's report mentions no clear grounds for President's Rule.
3. issued with mala fide intention


-----
