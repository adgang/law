# Syllabus
(as per [OU Website](http://www.osmania.ac.in/lawcollege/syllabi.html))

## PAPER–V: ENVIRONMENTAL LAW

## Unit-I
The  meaning  and  definition  of  environment  –  Ecology  -  Ecosystems-Biosphere  -  Biomes  -  Ozone depletion  -  Global  Warning  -  Climatic  changes  -  Need  for  the  preservation,  conservation  and  protection  of environment  - Ancient  Indian  approach  to  environment- Environmental  degradation  and  pollution  - Kinds,  causes and effects of pollution.

## Unit-II
Common Law remedies against pollution - trespass, negligence, and theories of Strict Liability & Absolute Liability  - Relevant  provisions  of  I.P.C.  and  Cr.P.C.  and  C.P.C.,  for  the  abatement  of  public  nuisance  in  pollution cases - Remedies under Specific Relief Act - Reliefs against smoke and noise - Noise Pollution.

## Unit-III
The  law  relating  to  the  preservation,  conservation  and  protection  of  forests,  wild  life  and  endangered species, marine life, coastal ecosystems and lakes etc. - Prevention of cruelty towards animals - The law relating to prevention and control of water pollution - Air Pollution - Environment pollution control mechanism - Law relating to environment protection – National Environmental Tribunal and National Environmental Appellate Authority.

## Unit-IV
Art.  48A  and  Art.  51A(g)  of  the  Constitution  of  India  -  Right  to  wholesome  environment  -  Right  to development -  Restriction on freedom of trade, profession, occupation for the protection of environment - Immunity of Environment legislation from judicial scrutiny(Art.31C) - Legislative powers of the Centre and State Government -  Writ jurisdiction - Role of Indian Judiciary in the evolution of environmental jurisprudence.

## Unit-V
International  Environmental  Regime  - Transactional  Pollution  - State  Liability  - Customary International Law - Liability of Multinational Corporations/Companies - Stockholm Declaration on Human Environment, 1972 - The  role  of  UNEP  for  the  protection  of  environment  -  Ramsar  Convention  1971  –  Bonn  Convention  (Migratory Birds) 1992 -  Nairobi Convention, 1982 (CFCC) - Biodiversity Convention (Earth Summit), 1992 -  Kyoto Protocol 1997, Johannesburg Convention 2002.

## Suggested Readings:
1. Paras Diwan: Studies on Environmental Cases.
2. S.N. Jain (ed.): Pollution Control and the Law.
3. Armin Rosencranzand Shyam Divan: Environmental Law and Policy in India.
4. A.Agarwal (ed.): Legal Control of Environmental Pollution
5. Chetan Singh Mehta: Environmental Protection and Law
6. V.K. Krishna Iyyer: Environment Pollution and Law
7. Shah: Environmental Law
8. Paras Diwan: Environmental Law and  Policy in India
,1991
9. Dr. N. Maheshwara Swamy, Environmental Law, Asia Law House, Hyderabad

# Study Notes

## Wed 14 Mar 2018 20:43:28



-------
