Mon 12 Feb 2018 13:22:58
-------

# Environment

1. Forest Act one question in Part C 1855- first forest Act
2.


The first forest charter was in 1855. The first Indian Forest Act was in the year 1865, which was amended in 1878 and again in 1927.

Protected forest
Reserved forest
1878 - effective bifurcation in 1927. Protected forest cannot be shifted to reserve forest after 1927.

Salient features of 1927 Indian Forest Act:
1. Division of forests:
    a. Reserve forests - for development in the interest of public
    b. protected forests -
    c. Village forests -  Communities and Tribals interests are at stake. So they were allocated forest for livelihood activities. They cannot mortgage, sell this land.
    d. Private Forest - relating to transfer or deforestation. All other management aspects can be taken by owner


Objective of this act is to consolidate the laws relating to forests. The transit of forest produce and the duty leviable on timber and other forest produce like sandalwood.

Part V of Indian Forest Act 1927 -

Only 13% forest cover in India.
Reserve forest is the most restricted category which is constituted by the state government on any forest land, with the help of forest settlement officer, who settles the claims in the forest area. He/she has to record all claims with proofs and process them with compensation.

State government is empowered to declare an area as protected forests for the purpose of conservation or to protect forests which were destroyed by fire or any other disaster. These forests can be closed down for a period of 35 years. All the matters of giving permissions related to reserve forest such as right to way, right to water, minor forest produce will be taken up by discretionary powers of District Collector.

Village Forests - The state government may assign to any village community the rights of government over any land which has been constituted as a reserve forest. Any kind of allocation of these village forests is prohibited.

Private forests - were regulated by the government in the areas of clearing of the forests and transferring ownership rights. The benefits arising from private forests were completely taken by the owners of the forests. The power to fix duty on timber and other forest produce lies with the state government.

Offenses in forest area:
1. Felling the trees - deforestation without permission
2. A person who commits offenses relating to cutting down trees or setting forest on fire or permits cattle to damage forests shall be punishable with imprisonment which may extend to 6 months or with fine which may extend to Rs 500 or both.
Any forest officer can arrest a person on suspicion of a forest offense without an order of the magistrate. 3 or 4 days is the allowed time to produce the person before a court.

Any forest officer or police officer who unnecessarily sieges any property shall be punishable with imprisonment which may extend to 6 months or with fine which may extend to Rs500 under Section 62 of the Act.

75 Sections in the Act, but simple to under the Act part wise.

Whoever knowingly or negligently destroys or defaces any mark on a tree, or boundary mark in a forest, they are punishable with imprisonment for a term which may extend to 2 years or with fine or both under Section 63 of the Act.

District Magistrate or any first class magistrate has the power to try the cases summarily.

Under the code of criminal procedure 1898, the punishment here is not exceeding six months imprisonment or fine of Rs 500 or with both for forest offenses.
under Section 67  
596 cases in AP - related to sandalwood smuggling etc.  
AP forest act 1967 - follows 1927 Act

Rights of foresters Act

Forest Conservation Act

Compulsory question on Forest Act





----
