Fri 08 Dec 2017 01:22:59 PM
----

# Envrionment

## Water Pollution

Water pollution is the addition of certain substances whether organic, inorganic, biological or radiological factors which substitute and degrade the quality of water so that using the water becomes hazardous to health.

### Inorganic Pollutants and organic Pollutants

Inorganic pollutants include metals, chlorides, oxides of iron, copper, mercury.

Organic pollutants include cellulose, fats, organic acids, proteins and oils which are not good for health.



### Sources of Water Pollution

1. Sewage - discharge into canals, rivers and water bodies
2. Pesticides and Fertilisers - mixing in water bodies
3. Dumping of industrial waste - in water bodies exposing children, cattle and birds.
4. Mining
5. pharmaceutical industry
6. soap and detergent
7. paper and pulp
8. sprays and solvents for metal cleaning and car cleaning


### Effects of Water Pollution
Water pollution leads to:
1. diseases such as gastroenteritis, cholera, typhoid, jaundice, stomach ailments
2. ground water pollution diseases like florosis, skeletal florosis, which causes teeth deformity, bone problems, painful joints.

    Blue baby syndrome is caused by excess nitrate in water, drinking which reduces oxygen content in the body by reacting with haemoglobin. And it is fatal for infants.
3. `Eutrophication` - the process of nutrient enrichment of water which often lead to the loss of species and bio diversity in the aquatic ecosystem by creating imbalance in the ecosystem through the profuse growth of algae( like blue green algae which is also toxic)
4. `Biomagnification` - the phenomenon through which certain pollutants get accumulated in increasing concentration along the food chain is called biological magnification.


### Control of Water Pollution

Stages of controlling water pollution.  
1. Primary
2. Seconday
3. Tertiary


Secondary level treatment and tertiary level treatment(in crores) is much costly. Ganga is getting tertiary level treatment.

1. Primary treatment stage consists of sedimentation and filtration.
2. Secondary treatment stage such as biological oxidation by the formation of sludges
3. Tertiary treatment stage is a physico chemical process which involves chemical oxidation of waste water by strong oxidising agents such as chlorine gas. perchlorate salts and ozone gas and ultraviolet radiation.


Tomorrow - soil and noise pollution


----------------
