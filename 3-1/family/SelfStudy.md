# Syllabus

PAPER-II: FAMILY LAW–I (Hindu Law)

## Unit-I
Sources of Hindu  Law – Scope  and application of Hindu  Law – Schools  of Hindu  Law - Mitakshara  and Dayabhaga  Schools  –  Concept  of  Joint  Family,  Coparcenary,  Joint  Family  Property  and  Coparcenary  Property  – Institution  of  Karta-   Powers  and  Functions  of  Karta  -  Pious  Obligation  -  Partition  –  Debts  and  alienation  of property.

## Unit-II
Marriage  -  Definition  -  Importance  of  institution  of  marriage  under  Hindu  Law  –  Conditions  of  Hindu Marriage – Ceremonies and Registration – Monogamy – Polygamy.

## Unit-III
Matrimonial Remedies under the Hindu Marriage Act, 1955 - Restitution of Conjugal Rights – Nullity of marriage – Judicial separation – Divorce – Maintenance pendente lite
– importance of conciliation.

## Unit-IV
Concept  of  Adoption  - Law  of  Maintenance  - Law  of  Guardianship  -  Hindu  Adoption  and  Maintenance Act, 1956 – Hindu Minority and Guardianship Act 1956.

## Unit-V
Succession – Intestate succession – Succession to the property of Hindu Male and Female; Dwelling House – Hindu Succession Act, 1956 as amended by the Hindu Succession (Andhra Pradesh Amendment) Act, 1986 & the Hindu Succession (Amendment) Act, 2005 – Notional Partition – Classes of heirs – Enlargement of limited estate of women into their absolute estate.

## Suggested Readings:
1. Paras Diwan: Modern Hindu Law, 13th Edition 2000, Allahabad Agency, Delhi.
2. Paras Diwan: Family Law, 1994 Edition, Allahabad Agency, Delhi.
3. Mayne: Hindu Law - Customs and Usages, Bharat Law House, New Delhi.
4. Sharaf: Law of Marriage and Divorce, 1999.


# Study Notes

## Tue 06 Mar 2018 04:01:35



Acts:
1. Hindu Marriage Act, 1955
2. Hindu Adoption and Maintenance Act, 1956
3. Hindu Minority and Guardianship Act 1956
4. Hindu Succession Act, 1956 as amended by the Hindu Succession (Andhra Pradesh Amendment) Act, 1986 & the Hindu Succession (Amendment) Act, 2005









  Grounds for Divorce:

  Section 13  
  1) Fault theory  
  i) Adultery  
    ia) Cruelty  
    ib) Desertion for not less than 2 years immediately preceding petition  
  ii) Ceased to be Hindu by conversion to another religion  
  iii) incurably of unsound mind, or suffering continuously or intermittently from mental disorder kind and extent petitioner cannot live with respondent  
  iv) virulent and incurable form of leprosy  
  v) venereal disease in communicable form  
  vi) has renounced the world by entering a religious order  
  vii) has not been heard of as being alive for a period of seven years or more by those persons who would have heard of it had the party been alive  


1A) Either party to the marriage, whether solemnized before or after the commencement of this Act, may also present a petition for the dissolution of the marriage by a decree of divorce on the ground-  
i) that there has been no resumption of cohabitation as between the parties to the marriage for a period of one year or upwards after the passing of a decree for judicial separation in a proceeding to which they were parties or  
ii) that there has been no resumption of cohabitation as between the parties to the marriage for a period of one year or upwards after the passing of a decree for restitution of conjugal rights in a proceeding to which they were parties.  
[has to be immediately preceding the petition]


2) A wife may also present a petition of divorce for dissolution of marriage by a decree of divorce on the ground-  
i) in the case of marriage solemnized before the commencement of this Act; that the husband had married again before the commencement, or any other wife of the husband married before such commencement was alive at the time of the solemnization of the marriage of the petitioner:  
provided in either case the other wife is alive at the time of presentation of the petition;  
ii) that the husband has, since solemnization of the marriage been guilty of rape, sodomy or bestiality; or  
iii) that in a suit under Section  18  of  the  Hindu  Adoptions  and  Maintenance  Act, (78  of  1956),  or  in  a  proceeding  under  Section  125  of  the  Code  of  Criminal Procedure, 1973, (Act 2 of 1974) or under corresponding Section 488 of the Code of Criminal Procedure, (5 of 1898), a decree or order has been passed against the husband awarding maintenance to the wife, notwithstanding that she was living apart, and since the passing of that decree or order, cohabitation has not resumed for one year or upwards,  
iv) that her marriage(consummated or not), was solemnized before she attained 15 years and she has repudiated the marriage after attaining that age and before attaining the age of 18 years  

13-A - Alternate remedies - except for ii, vi, vii
pass a decree of judicial separation instead  
13-B - Divorce by mutual consent  
1. PDMDD on the ground that they have been living separately for a period of one year or more, that they have not been able to live together and that they have mutually agreed that the marriage should be dissolved
2. motion of both parties, 6-18 months on above petition, not withdrawn, the court upon being satisfied, after hearing parties and after making such inquiry as it thinks fit,  
    * that a marriage has been solemnized
    * and that averments in the petition are true,  
pass a decree of divorce declaring the marriage to be dissolved with effect from the date of decree.


Section 14 - 1 year elapsed since marriage
rules made by HC
exceptional hardship or exceptional depravity  
if leave to present petition is obtained by misrepresentation or concealment of the nature of the case, if it pronounces the decree, may do so subject to the condition that the decree shall not have effect until after the expiry of one year from the date of the marriage or dismiss the petition without prejudice to any petition which may be brought forward after the expiration of the said one year upon the same or substantially the same facts as those alleged in support of petition.  


Section 15 - when can they marry again.

Section 16 - legitimate and illegitimate children
1. Section 11 whether or not decree or nullity is granted under this act, whether or not the marriage is held to be void otherwise than on a petition under this Act.
2. Section 12 - whether or not decree or nullity is granted under this act
3. Only parents property

Section 17 - punishment for bigamy - 494 and 495 of IPC  




Maintenance:
1. Section  18  of  the  Hindu  Adoptions  and  Maintenance  Act, (78  of  1956),
2. Section  125  of  the  Code  of  Criminal Procedure, 1973, (Act 2 of 1974)
3. Section 488 of the Code of Criminal Procedure, (5 of 1898)


Section 5.
Conditions of Valid marriage:
i. neither party has a spouse living at the time of the marriage  
ii. at the time of marriage, neither party -  
    a) is incapable of giving valid consent to it in consequence of unsoundness of mind  
    b) though capable of giving consent, has been suffering from mental disorder of such a kind or to such an extent as to be unfit for marriage and procreation of children  
    c) has been subject to regular attacks of insanity or epilepsy  
iii. the bridegroom has completed the age of 21 years and the bride has completed the age of 18 years  
iv. the parties are not within degrees of prohibited relationship, unless the custom or usage governing each of them permits of a marriage between the two  
v. the parties are not sapindas of each other unless the custom or usage governing each of them permits of a marriage between the two  


Section 11
Any marriage solemnized after the commencement of this Act shall be null and void and may, on petition presented by either party thereto, be so declared by a decree of nullity if it contravenes any one of the conditions specified in clauses i, iv, v of Section 5.


Section 12
1. before or after
    shall be voidable and may be annulled by a decree of nullity on any of the following grounds
    a. marriage not consummated owing to the impotency of the respondent or
    b. that the marriage is in contravention of clause(ii) of section 5



-----
