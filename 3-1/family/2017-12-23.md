Sat 23 Dec 2017 11:33:56 AM
-----

# Family

## Section 14

One year bar to divorce of fair trial truth.

Under Section 14, a petition for divorce or judicial separation cannot be filed within one year of marriage.

This is known as fair trial rule.This is based on the assumptoion that it is the duty of every married person to give fair trial to the marriage.

In exceptional case, where exceptional hardship or exceptional depravity on the part of the respondent, the marriage may be dissolved within one year of solemnization o f marriage.

Execptional hardship means husband is guilty of two or more matrimonial offenses.


## Section 15
Divorce person can marry again.

When decree of divorce is passed and there is no right of appeal against the decree.

Or time for appeal has expired without an appeal having been presented or appeal has been presented but has been dismissed.

The either party can remarry.



## Section 16


Legitimacy of void and voidable marriage
The children of void and voidable marriage are legitimate but they will be inheriting only the father's separate property.


## Section 17 - Punishment for Bigamy

Any narriage between two Hindus solemnized after c

the act is void if at

of such marriage, eitherr party had a husband or wide livi



494 and 495 of IPC (45 of 1960) shall apply.

## Section 18

Punishment for contrvention of certain other conditions of Hindu marriage

clauses iii, iv, v of section 5 are punishable:

simple imprisonment which may not exyend one mnth or with fine upto Rs 1000 or both.

## Section 19 - Jurisdiction and Procedure

1. where the marriage was solemnized
2. the respondent at the thme of the presentation resides
3. the parties to the marriage last resided together
4. the  petitioner  is  residing  at  the  time
of  the  presentation  of  the  petition,  in  a
case where the respondent is at that time
, residing outside the territories to which
this  Act  extends,  or  has  not  been  heard
of  as  being  alive  for  a  period  of  seven
years


## Section 20
Contents and verification of Petitions

The content of petition  a


## Section 21

CPC provisions will apply;

If judicial separation and
the second petition will be sent to filed wh

If petitions are filed in the same court, they should be clubbed.

## Section 22

Proceedings to be in camera and may not be printed or published.

If the proceedings are published, then Rs1000 fine will be imposed.


## Section 23

Subclause 1.
Last matrimonial

1. Standard of two
2. Taking advantage of once own wrong or disablilty


Accessory, connivance, collusion, delay

ii) Reconciliation
The act enforces a duty on the court to affect a reconcilion between partes. Reconciliation proceedings are not applicable when petition for divorce or judicial separation s on the ground of conversion, insanity, leprosy, VDs, renunciation of world or presumption of death.


## Section 24 - Maintenance Pendente Lite

Maintenance during the pendency of proceedings is known as maintenance pendente lite or   interim maintenance or temprorary maintenance.

Maintenance can be filed in trial court or appellate court.


Maintenance can be claimed by petitioner or respondent.



Plain image made for personal maintenance of the claimant and exps]ense

Soluld establish that claimant has no independent income sufficient for his/her independence.


Against an interim order, no appeal is maintenable. A revision can be filed(in HC). The application for maintenance should be disposed off within 60 days of service of no days of the wife or husbad.


## Section 25 - Permanent maintenance and alimony

On the application of either spouse, the court may pass an order for permanent alimony and maintenance.

At the time of passing of the decree granting the petition or at any time after the passing of the decree granting the petition,

ii) Variation of the order: The court has power to vary, modify or discharge any order of permanent alimony at the instance of either party if change of circumstances is shown.

Ex: prolonged treatment for a chronic disease.

3. When an order may be rescinded:

    * if he or she remarries
    * if she/he hasn't remained chaste or he has sexual intercourse with any woman.
    * resumption of cohabitation by the parties after maintenance order


On the death of the non-claimant, the order of maintenance comes to an end.


## Section 26 - Custody of Children

Court may be called upon to decide custody and maitenance of children during the matrimonial proceedings or at the time of passing the decree or subsequent to the passing of the decree when no application is filed or when the court has already passed orders to get that order modified or cancelled or changed.

The order terminates on children gaining majority.

## Section 27 - Provisions in respect of joint property

The court may make such provisions in the decree with respect to any property presented at the time of marriage or later on which may belong jointly to both the husband and wife.

## Section 28

Appeal shall lie within 90 days from the order. No appeal on the subject of costs only.

## Section 28 (A) - Enforcement of decrees and orders
The decrees and orders of the court are made in exercise of its original civil jurisdiction.

## Section 29 - Savings Clause

The customs are saved

## Section 30 - Repealed in 1960

-----------------------
