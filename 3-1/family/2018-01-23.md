Tue 23 Jan 2018 11:53:32 AM
----

# Family


#### Example:
1. A has two sons B and C. C died after having two daughters D1 and D2

A died leaving behind property. What is the property distribution?
B - 50%
D1 - 25%
D2 - 25%




4. Doctrine of representation


Class II heirs are 9 entries with 23 categories

1. father(In mitakhsara mother is closer and in class I father is in class II)
2. a. Son's daughter's son  
    b. Son's daughter's daughter  
    c. brother  
    d. sister  
3. a. Daughter's son's son  
    b. daughter's son's daughter
    c. daughter' daughter's son  
    d. daughter's daughter's daughter.
4. a. Brother's son  
    b. sister's son  
    c. brother's daughter
    d. sister's daughter.
5. Father's father; father's mother
6. Father's widow; brother's widow
7. Father's brother; father's sister
8. Mother's father; mother's mother
9. Mother's brother; mother's sister


A died intestate(without writing a will) leaving behind 10lacs.
Survived by mother, widow, son, brother, sister, father's father, father's mother

Only mother, widow and son get 1/3rd each because they are in class I.


---------------
