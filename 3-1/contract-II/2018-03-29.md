Thu 29 Mar 2018 10:29:17 AM
-------

# Contract II



Liability of surety arises immediately on the default of the principal debtor.

#### Bank of Bihar Vs Damodar Prasad

[Bank Of Bihar Ltd vs Damodar Prasad & Anr](https://indiankanoon.org/doc/743049/) on 8 August, 1968
Equivalent citations: 1969 AIR 297, 1969 SCR (1) 620
Parasram

When the plaintiff bank challenged the validity of the condition in the decree(to proceed against the principal debtor first), Supreme Court set aside the condition and allowed the bank to proceed against the surety also.

Prior action against the principal debtor is not necessary.

#### Ram Bahadur Singh Vs Tehsildar And Ors

[Ram Bahadur Singh And Anr. vs Tehsildar And Ors.](https://indiankanoon.org/doc/823346/) on 24 July, 2002  
Equivalent citations: 2003 (1) ARBLR 61 All

Guarantor cannot insist that the creditor first proceed against the principal debtor.

Prior Action against pledged goods is not necessary.

#### State Bank of India Vs Gautmi Devi Gupta

[State Bank Of India vs Smt. Goutmi Devi Gupta And Ors.](https://indiankanoon.org/doc/111232/) on 15 October, 2001  
Equivalent citations: AIR 2002 MP 81, 2002 (1) MPHT 427

1. Lean
2. Pledge(security will be with lender)
3. Hypothecation(movable - not recognised by ICA, security will be with borrower)

Even without proceeding against hypothecated goods, the bank can proceed against the surety, because the liability of principal debtor and surety is joint and several. Surety will not be liable if the creditor has obtained guarantee by misrepresentation with regard to material part of the transaction.

Law treats two separate contracts from the point of view of creditor. Therefore under certain circumstances, one is liable without making the other liable.

a) If some variation is done by the creditor, surety is discharged by the principal debtor while principal debtor continues to be liable.  
b) An admission by a principal debtor is not an evidence against surety. A judgement obtained against principal debtor is not enforceable against the surety.  
c) Debt becomes time-barred as against the principal debtor but the surety may still be liable.  

Section 128.


## Rights of surety:

### Against creditor:
1. Right to benefit of creditors securities - Section 141
Right of subrogation - surety enters shoes of creditor once he pays off the debt. If the security is lost at creditor due to negligence, surety will be discharged of debt to the extent of the value of security.

Surety is entitled to demand the security from the creditor at the time of making the payment. WHether he knows or not about the security

If by negligence of the creditor, the security is lost, any has been transferred by creditor would be discharged to that extent. When the security is lost due to the Act of God, or the enemies of the state, surety continues to be liable.

Right to claim setoff if any:

### Against principal debtor:
### Against co-sureties

------
