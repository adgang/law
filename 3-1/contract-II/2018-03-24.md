Sat 24 Mar 2018 11:40:07 AM
-------
# Contract II

In a contract of guarantee, there must be three parties - principal debtor, creditor and surety

S says to a shopkeeper - 'Let P have the goods and if he does not pay, I will pay'.


Three purposes:
1. Obtain loan
2. goods on credit
3. procure employment

Creditor may ask another security called collateral security. In certain cases, surety may come without collateral. If there is a collateral security, the loan is called a secured loan.

Essentials(features) of contract of guarantee:
1. concurrence of all the three parties
2. primary liability in some person
3. essentials of a valid contract - consideration received by principal debtor is sufficient to support surety's promise. Section 127
4. Contract of guarantee may be oral or in writing. Under English law, it must be in writing, signed by the party to be charged.

Contract of guarantee is not a contract of fidelity which requires full disclosure of material facts.


Differences between indemnity and guarantee:

| Indemnity       | Guarantee
|---|
|  A contract by which one party promises to save the other from loss caused to him by the contract of the promisor himself, or by the conduct of any other person, is called a "contract of indemnity".  | A "contract of guarantee" is a contract to perform the promise, or discharge the liability, of a third person in case of his default. The person who gives the guarantee is called the "surety", the person in respect of whose default the guarantee is given is called the "principal debtor", and the person to whom the guarantee is given is called the "creditor". A guarantee may be either oral or written.
| Contract of indemnity is based on contingency | Contract of guarantee is based on existing debt
| In indemnity there are only two parties | Guarantee has three parties
| Liability of indemnifier is primary | Liability of surety is secondary and contingent
| If the indemnifier wants to sue, he has to sue in the name of indemnified or the right must be assigned to him by the indemnified | If the principal debtor makes a default, surety discharges the liability and then enters into the shoes of creditor. Therefore he can directly file a suit on the third party in his own name.


Section 128 - Nature of surety's liability

The liability of surety is co-extensive with that of the principal debtor, unless it is otherwise provided by the contract.

His liability is secondary and contingent.

1. Surety is liable for what principal debtor is liable. But a surety may limit his liability by a special agreement.
2. Law treats two separate contracts in a contract of guarantee from creditor's point of view. This is based upon several judicial pronouncements. As per law, the creditor can proceed against surety only after exhausting all his remedies against the principal debtor. Without a demand and default from the principal debtor, the creditor cannot proceed against surety.

Law deduced from judicial pronouncements:
1. Surety may be liable even though the principal debtor is not liable. Example: when the principal debtor is a minor.
2. Sometimes a debt may become time-barred against principal debtor - surety continues to be liable if he keeps his debt alive either by payment of interest or part payment of principal sum within that period.
3. Under Section 145, surety can recover from the principal debtor when he has rightfully paid the amount.
4. Discharge of principal debtor by operation of law does not discharge a surety.
5. If liability of principal debtor is scaled down under Debt Relief Act, surety's liability is also reduced.
6. If the creditor gets a decree against both principal debtor and surety, creditor may execute against the surety only.
7. Contract between principal debtor and creditor is voidable and therefore it is set aside by principal debtor - surety will not be discharged.
8. Contract between principal debtor and creditor is void. Surety will continue to be liable.

------
