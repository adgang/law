Mon 13 Aug 2018 10:28:39 AM
-------

# Contract-II

== Notes by Uma


## Sale of Goods Act 1930

Sale: A contract where seller transfers or agrees to transfer the property in goods to the buyer.

Essential Characteristics of a contract of sale:
1. Two parties
2. Transfer of property
3. Goods
4. Price
5. It includes both sale and agreement to sell
6. No legal formalities need to be observed






----
