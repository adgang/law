
The case here is that of a girl whose consent for marriage was obtained by concealment of a material fact - which is that the boy is incapable of sexual intercourse with the girl - whether by choice or capacity.
