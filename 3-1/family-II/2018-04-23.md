Mon 23 Apr 2018 09:53:39 AM
-------

# Family-II

## Schools of Islam

### Hanbali School

Musnad

Rasul Kitab Ul Alan

Saudi Arabia and Qatar are the places where Hanbali school prevails. Some followers were there in Persia, Baghdad and Damascus.


## Shia Schools of Law

Imam Jafar Sadiq founded the Shia school. Hence the Shia school is earlier than the Sunni schools. As has been seen earlier, the Shias do not accept any tradition which doesn't emanate or originate from the household of the prophet, particularly of Ali.

Abu Bakr was first Caliph, father-in-law of the Prophet.


Omar, Usman, Ali(son-in-law and cousin of Prophet)

Shias do not accept Kiyas. According to them, the Imam is the final interpreter of law. The Ijma would be valid only when it was not possible to consult the Imam.


Shias do not accord recognition to equity, public policy, public good or analogy. Shia means faction. It represents that faction of muslims which, after the death of the Prophet attached itself to Ali, the son-in-law of Prophet, and considered him to be their leader both in temporal and spiritual matters.

The Shias consider Ali to be the rightful successor to the Prophet and consequently do not recognise the first three Caliphs. The Shias had been continuously plagued by the dynastic troubles. The first dynastic trouble arose after the death of the fourth Imam [Zayn al-Abidin](https://en.wikipedia.org/wiki/Ali_ibn_Husayn_Zayn_al-Abidin). One faction of Shias accepted Said, the son of fourth Imam as their Imam. Thus came into existence of the first school of Shias Zaydis.

Yemen is the stronghold of Zaydis.

Zaydees Ismailis Isnacharis?

After the death of Imam Zafar, the majority of Shias followed Musa Qazim, the founder of Ithnā‘ashariyyah school, which is also known as Twelvers. Iran and Iraq belongs to this sect, Syria, Lebanon and Pakistan also has followers of this school.

https://en.wikipedia.org/wiki/Twelver
https://en.wikipedia.org/wiki/Musa_al-Kadhim



The minorities of Shias after the death of Zafar followed Ismail, the elder son of Imam Zafar, the founder of Ismaili school. They are also known as Seveners. This sect is found in Egypt, some part of Central Asia, East Africa, South Arabia, Iran, Syria and Pakistan.


In India, Ismailis are divided into Khojas and Bohras. This bifurcation is specific to India. Section II of 1937 names them among five sects and exempts them from joint family, succession customs.

Tomorrow new chapter.


-----
