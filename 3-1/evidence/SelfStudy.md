# Syllabus
(as per [OU Website](http://www.osmania.ac.in/lawcollege/syllabi.html))

## PAPER-IV: LAW OF TORTS INCLUDING MOTOR VEHICLE ACCIDENTS AND CONSUMER PROTECTION LAWS

## Unit-I
Nature of Law of Torts - Definition of Tort - Elements of Tort - Development of Law of Torts in England and India - Wrongful Act and Legal Damage - Damnum Sine Injuria and Injuria Sine Damnum - Tort distinguished from  Crime  and  Breach  of  Contract  - General Principles  of  Liability  in  Torts  - Fault  - Wrongful  intent  - Malice  - Negligence - Liability without fault - Statutory liability - Parties to proceedings.

## Unit-II
General Defenses to an action in Torts – Vicarious Liability - Liability of the State for Torts – Defense of Sovereign Immunity – Joint Liability – Liability of Joint Toreadors – Rule of Strict Liability (Ryland’s V Fletcher) – Rule  of  Absolute  Liability  (MC  Mehta  vs.  Union  of  India)  – Occupiers  liability  – Extinction  of  liability  – Waiver and Acquiescence – Release – Accord and Satisfaction - Death.

## Unit-III
Specific  Torts  -  Torts  affecting  the  person  -  Assault  -  Battery  -  False  Imprisonment  -  Malicious Prosecution - Nervous Shock - Torts affecting Immovable Property - Trespass to land - Nuisance - Public Nuisance and Private Nuisance - Torts relating to movable property – Liability arising out of accidents (Relevant provisions of the Motor Vehicles Act).

## Unit-IV
  Defamation - Negligence - Torts against Business Relations - Injurious falsehood - Negligent Misstatement - Passing  off  -   Conspiracy  - Torts  affecting  family  relations  - Remedies  -  Judicial  and  Extra-judicial  Remedies – Damages – Kinds of Damages – Assessment of Damages – Remoteness of damage - Injunctions - Death in relation to tort - Action personalize moritur cum persona.

## Unit-V
Consumer  Laws:  Common  Law  and  the  Consumer  -  Duty  to  take  care  and  liability  for  negligence  - Product  Liability  -  Consumerism  -  Consumer  Protection  Act,  1986  -  Salient  features  of  the  Act   -   Definition  of Consumer - Rights of Consumers - Defects in goods and deficiency in services – Unfair trade practices - Redressal Machinery under the Consumer Protection Act - Liability of the Service Providers, Manufacturers and Traders under the Act – Remedies.

## Suggested Readings:
1. Winfield & Jolowicz: Law of Tort, XII edition, Sweet and Maxwell, London , 1984.
2. Salmond and Heuston: Law of Torts, XX edition, 2nd Indian reprint, Universal Book traders, New Delhi,1994.
3. Ramaswamy Iyyer: The Law of Torts, VII edition (Bombay, 1995).
4. Achutan Pillai: Law of Tort, VIII edition , Eastern Book Company, Lucknow, 1987.
5. Durga Das Basu: The Law of Torts,X edition, Prentice Hall of India, New Delhi, 1998.
6. Ratan Lal & Dhirajlal: The Law of Torts, 22nd edition, Wadhwa & Company Nagpur, 1992.
7. R.K.Bangia: Law of Torts, XIV edition, Allahabad Law Agency, Allahabad, 1999.
8. J.N.Pandey: Law of Torts, 1st edition Central Law Publications, Allahabad, 1999.
9. Vivienne Harpwood: Law of Torts, 1st edition, Cavendish Publishing Ltd. London, 1993.
10. Hepple & Mathews: Tort - Cases and Materials, 2nd edition  Butterworth, London, 1980.
11. D.N.Saraf: Law of Consumer Protection in India, Tripati, Bombay The Motor Vehicles Act, 1988

# Study Notes

## Mon 12 Mar 2018 06:27:28


1. When will the peace deal be signed? Now everywhere there is BJP govt, what is stopping them?
2. When will highway 'taxation' end?
3. When

Consumer  Protection  Act,  1986  
The Motor Vehicles Act, 1988  


-------
