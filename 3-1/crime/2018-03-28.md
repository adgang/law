Wed 28 Mar 2018 11:35:47 AM
-------

# Law of Crimes

Theories of crime:

What are the reasons for deviation of society causing crimes?
criminologists, jurists and economists developed theories. sociologists, psychologists.

Classical theories of crime:
Many scholars, reformists, jurists, legislators have given their own explanation for crime and ideas for its prevention. What are the causes for man to violate law(social)?

What causes a specific individual to break a social sanction or law or custom?
The answer was given by examining the causes propounded by eminent jurists.

Three scientific theories which explain criminal conduct:
1. Crime is the result of a person succumbing to the allurements of evil spirits.
This view flourished in primitive, medieval societies. Since it was thought that evil spirits infested a person and had to be driven out. The conventional notion of it was either to exorcise the evil spirit or to get rid of the possessed person by death or exile. This mostly existed among tribals - in Orissa, MP, Bihar, Nagaland etc.
2. Classical school theory: According to this theory a man's behaviour is governed by consideration of pain and pleasure. The pleasure contemplated from a particular act may be balanced against the pain anticipated from the same act. Penalty should be severe, so pain should exceed the pleasure from the violation of the law.
3. Moral incentive: This theory was very popular in 19th century. Crime is defined as moral derangement as a state of moral principle with vicious actions through the instrument of passion. Crime committed under influenced of blind impulse which was irresistible.
4. Psychological theory: It deals with the driving forces of human conduct in order to explain criminality on the part of individual.

There are two theories of crime:

## Theories of punishment:
1. Punishing authority was transferred from the individual to the society and to the state. Two wings of the state has been created - police and courts. The state should not interfere with the affairs of the courts so that they can give impartial judgements and decisions.
2. Almost all countries have enacted a statute imposing punishments for wrongdoers. There may be differences in imposing punishment. Prostitution is not an offense in Western countries. The object of law is to reform the criminal as to prevent him from repeating. Crime should be properly diagnosed and treated scientifically. Punishment must not be regarded as the end but only as a means to an end. The end reclamation of the criminal to useful citizen.

### Deterrence theory
 Deter means abstain from action. Deterrent punishment means severe punishment, intended to prevent the offender from again committing the crime. The object of this theory is to put a person in fear and thus abstain from criminal behaviour. This imposition of strict punishment depending upon the nature of offense like capital punishment, forfeiture of property, imprisonment etc. The main purpose of this theory is that severe punishment will deter the remaining people with fear to commit such crime. Punishment should be implemented openly.

At least most of the society will be happy.


### Retributive theory
Recompose punitive, 'to payback' ie to return the same injury to the wrongdoer which he has committed against the victim. Tooth for tooth, eye for eye, vengeance of the state.




------
