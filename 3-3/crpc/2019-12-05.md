Thu 05 Dec 2019 12:27:22 PM
-----

# Criminal Procedure Code

###  IV Proclamation for person absconding
Chapter VI C
Section 82 - 86

min 30 days

1. Proclamation is an order issued by the court directing the accused to appear before it within the time given in that proclamation to answer for the allegation pending against him - not less than 30 days time should be given to the accused to appear before the court.
2. Proclamation is issued by the court when the previous processed failed in bringing the accused before the court.
3. When proclamation is issued by the court, a copy of it must be affixed in the notice board of the court; another copy should be affixed in the conspicuous place of the accused.
4. Another copy should be published in the daily newspaper widely circulated in that area.
5. If necessary, proclamation should be announced in the place of the accused by adopting drumbeating.
6. When the proclamation is issued, accused person is expected to appear before the court within the time given in it.
7. When proclamation is issued for an offense under Sections 302, 304, 364, 367, 382, 392, 393, 394, 395, 396, 397, 398, 399, 400, 402, 436, 449, 459 or
460, and such person fails to appear at the specified place and time required by the proclamation, it may pronounce him proclaimed offender and make a declaration to that effect.

302 - murder
303 -

### V Attachment of property of person absconding
Section 83

Receiver law is in CPC

Movable property - receiver
Immovable property under DM

1. Order of attachment of property of the accused is the fifth process provided by CrPC for bringing the accused before the court.
2. The court issuing the proclamation under Section 82, by recording the reasons order the attachment of property belonging to the proclaimed person.
3. That property may be moveable or immovable property.
4. For attaching the property of the accused, court appoints the receiver as per the provisions of CPC.
5. At the time of issuing the proclamation, if the court is satisfied by affidavit or otherwise that the accused is about to dispose off the whole or any part of his property, it may order the attachment simultaneously with the issue of proclamation.
6. If the attached property is moveable and perishable in nature, it must be kept for auction sale immediately and the sale proceeds must be deposited in the bank as per the directions of the court.
7. If the property to be attached is immovable property, the attachment should be made through the collector of the district in which the land is situated.

### Section 84 - Claims and objections if any

If there are any claims or objections relating the acknowledgement of property under Section 83, the aggrieved person must raise them before the JM of first class at any time within 6 months.

If the court considers the claim to be valid, to that extent the property attached would be released.
But if the court considers the claim to be not tenable, it may dismiss the claim,
In such a situation, the aggrieved person may file a civil suit within one year from the date of disallowing the claim.


### Section 85 - Release, sale and restoration of attached property
1. If the proclaimed person appears within the time specified in the proclamation, the court shall make an order releasing the property from attachment
2. If the proclaimed person doesn't appear within the time given the proclamation, the property under attachment shall be at the disposal of the state government.
3. It shall not be sold until the expiration of 6 months from the date of attachment or until the claim is disposed offender Section 84, unless it is subjected to speedy and natural delay.
4. After 6 months, the property should be sold and the sale proceeds should be deposited in the bank or for directions of the court.
5. If within   2 years, from the date of attachment the proclaimed person is brought before the court and if he proves that he did not abscond, the money deposited to the bank would be restored to him.


If the proclaimed person deposit appears before the court, even after 2 years, the property deposited in the bank will go to the state exchequer, court directs the SP to register the case as an LPC(Long Pending Case) and it issues a fresh NBW against the accused person for his appearance before the court.

### Processes for compelling 


V series
D series
MO1 MO2

-----
