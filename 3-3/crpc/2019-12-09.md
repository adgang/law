Mon 09 Dec 2019 12:30:30 PM
------


# Criminal Procedure Code


### Section 98 - Power to restore the abducted females

1. forceful compulsion or inducement by deceitful means
2. victim must go from one place to another

Mere abduction is not punishable.

But kidnapping is punishable even for good intentions.

1. Whenever a female is abducted, DM or SDM or JM of first class can issue search warrant under Section 98.
2. On the basis of such search warrant, police officer conducts the search of that place suspected to have that woman.
3. If the police officer identifies the woman, he has to restore her back to her husband in case of a married woman or to her parents in case of an unmarried girl.
4. Here the complainant must file the complaint before the DM or SDM or JM of first class on oath.

Arrest without warrant,


179, 193 IPC - false evidence


179 - not giving evidence

193 - false evidence


Madras High Court - 2 people in a hotel room  
No offense


Immoral Acts prevention Act -


## Search without warrant

Sections 103, 165, 166, 153

### Section 103

Magistrate may direct search in his presence


2 EMs and 1 JM

Sections 154-176 - Investigation of police

1. Section 103 - Here magistrate means both executive as well as judicial magistrates.
2. But that magistrate must be competent to issue search warrant.
3. When such magistrate is physically present at the place where search has to be conducted, he can conduct the search without warrant or he may cause it to be conducted by others.
4. Here the offence may be cognizable offence or non-cognizable offence. It makes no difference.

This is Section 103


### Section 165 - search by a police officer


IO - CI in urban areas or SI in rural areas

IO investigation


1. This Section 165 authorizes the officer in charge of a police station to conduct the search without warrant.
2. This Section is part of the chapter of investigation. So police officer investigating a cognizable offence can conduct the search of a place without warrant.




Police Act allows police to stop and search.

Section 151 - person designing to commit a crime - can be arrested without warrant

Every encounter should be registered as a murder - SC


3. IO can conduct search of a place without warrant but before going to conduct the search he must record the reasons for the necessity of conducting that search.
2. Then the IO must send the record of reasons to the nearest magistrate having jurisdiction.
3. Search without warrant under this Section should be only specific search. General search should never be conducted without warrant.
4. Sometimes IO may not be able to go personally to conduct the search. Then he may depute his subordinate by giving him a written order to conduct the search on his behalf.
5. When subordinate officer conducts the search, he has to submit a report to the IO explaining in it the procedure followed by him while conducting the search.

### Section 166
When one police officer of one police station makes a request to the police officer of another police station to conduct search of a place in that other jurisdiction on his behalf, that other police officer may conduct search without warrant.

But the offence should be a cognizable offence.

### Section 153
Inspection of weights and measures

1. Any police officer can conduct search of a place without warrant.
2. But that place must be suspected to contain the false weights and measures.
3. After identifying such false weights and measures, that police officer must immediately send them to the nearest magistrate having jurisdiction.
4.


### Section 91 Process No 4


General












----
