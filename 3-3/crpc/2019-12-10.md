Tue 10 Dec 2019 12:39:44 PM
-----

# Criminal Procedure Code


### Section 153
Inspection of weights and measures

1. Any police officer can conduct search of a place without warrant.
2. But that place must be suspected to contain the false weights and measures.
3. After identifying such false weights and measures, that police officer must immediately send them to the nearest magistrate having jurisdiction.
4.


### Section 91 Process No 4


General provisions of search


Persons incharge of a closed place to allow search.

Free ingress should be provided for searching officer. in presence of 2 respectable persons of the locality.


Stock witnesses

Panch witnesses

Inquest - 2 witnesses - panch witnesses


Prem Shankar Shukla

200 cases - false witness

IPC Section 92 - Act done in good faith - protects lawyers and doctors

1. This section explains the procedure which should be followed by the officer conducting the search.
2. That search may be with warrant or without warrant.
3. Occupier of the place must provide all the reasonable facilities to the officer conducting the search.
4. Free ingress must be given by the occupier to the officer conducting the search.
5. If free ingress is not provided, the officer can break open the doors or windows of the place to make his entry possible.
6. Search should always be conducted in the presence of two respectable residents of that locality.
7. They are known as panch witnesses.
8. Before the commencement of the search, the occupier must be allowed to search the officer and the witnesses. It is to be done so as to identify whether any item has been brought by them from outside to plant it in the place where search has to be conducted.
9. If there are any purdanashin women at the place where search has to be conducted, officer must respectfully ask those women to vacate that place. In their absence only search should be conducted.
10. Search witnesses must be allowed to be present in the place where search is conducted. They should not be kept outside.
11. As per Supreme Court,  
    a. wherever possible search should be conducted only with warrant.  
    b. wherever possible search should be conducted only during daytime.  
    c. occupier or his nominee should be allowed to be present at the place where search is conducted.  
    d. whatever items are recovered during the time of search, a list has to be prepared. A copy of that list should be given to the occupier and his acknowledgement should be taken.  
    e. another copy should be sent to the nearest magistrate having jurisdiction.  
    f. The list prepared by the officer must be signed by the panch witnesses and the officer.  
    g. Search is conducted for the sake of witnesses.  

### Non-compliance of search procedure
When a search is conducted without procedure provided under Section 100 and if that violation of search procedure leads to grave injustice to the accused, the search is considered invalid. Otherwise it is declared as valid.


## Section 102 - Seizure

307 - 10 years

1. Police officers have got the power to seize any property which is suspected to have been stolen or which is connected to the commission of any offense.
2. Such police officer, if subordinate to the officer incharge of a police station, must report the seizure to that officer.
3. Every police officer acting under this section shall report the seizure to the magistrate having jurisdiction.
4. If the property seized is such that it cannot be conveniently transported to the court, or if there is difficulty in securing proper accommodation for the custody of such property, the police officer may give custody of that property to any person on his executing a bond undertaking to produce the property before the court as and when required.
5. The property seized if subjected to speedy and natural decay and if the owner is not known, it must be sold under the orders of SP and the sale proceeds must be deposited as per the directions of him.

## Bail

Section 436-450

bail petition - constructive

Meaning: Bail is the security shown by the accused to the court with the assurance that in case he is released, he is ready to appear before the court whenever called rom then onwards without fail; otherwise he is ready to forego the bail bonds executed by him.


Constructive res judicata is not applicable to bail petitions.

Section 11 - res judicata

 





------
