Thu 03 Oct 2019 01:21:14 PM
----

# Criminal Procedure Code






An Act to consolidate and amend the law relating to children alleged and found to be in conflict with law and children in need of care and protection by catering to their basic needs through proper care, protection, development, treatment, social re-integration, by adopting a child-friendly approach in the adjudication and disposal of matters in the best interest of children and for their rehabilitation through processes provided, and institutions and bodies established, hereinunder and for matters connected therewith or incidental thereto.


Hague Convention on Protection of Children and Cooperation in Inter-country Adoption(1993)

11th December, 1992 to the Convention on the Rights of the Child,


1. Convention on the Rights of the Child 1989
2. the United Nations Standard Minimum Rules for the Administration of Juvenile Justice, 1985(the Beijing Rules)
3. the United Nations Rules for the Protection of Juveniles Deprived of their Liberty (1990)
4. the Hague Convention on Protection of Children and Co-operation in Respect of Inter-country Adoption (1993)


1. Juvenile Justice (Care and Protection of Children) Act, 2000
2. Juvenile Justice (Care and Protection of Children) Act, 2015

“Board” means a Juvenile Justice Board


“best interest of child” means the basis for any decision taken regarding the child, to ensure fulfilment of his basic rights and needs, identity, social well-being and physical, emotional and intellectual development;


### Section 68 - CARA - Child Adoption Resource Authority

(8) “begging” means—
(i) soliciting or receiving alms in a public place or entering into any private premises for the purpose of soliciting or receiving alms, under any pretence;
(ii) exposing or exhibiting with the object of obtaining or extorting alms, any sore, wound, injury, deformity or disease, whether of himself or of any other person or of an animal;

(5) “aftercare” means making provision of support, financial or otherwise, to persons, who have completed the age of eighteen years but have not completed the age of twenty-one years, and have left any institutional care to join the mainstream of the society;




----
