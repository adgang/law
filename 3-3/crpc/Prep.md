# Acts

1. Code of Criminal Procedure, 1898(Act No. 5 of 1898)
2. Indian Penal Code, 1860(Act No. 45 of 1860)
3.
4. THE PROBATION OF OFFENDERS ACT, 1958(ACT NO. 20 OF 1958)
An Act to provide for the release of offenders on probation or after due admonition and for
matters connected therewith.

##  Code of Criminal Procedure, 1898

Act No. 5 of 1898

## Indian Penal Code, 1860
Act No. 45 of 1860

## THE PROBATION OF OFFENDERS ACT, 1958
ACT NO. 20 OF 1958


Authorities: probation officer

Sec 3: admonition
Sec 4: probation

### Admonition

* section 379 - theft(3 yrs)
* section 380 - theft in dwelling house(7yrs)
* section 381 - theft by clerk/servant of master's property(7 yrs)
* section 404 - dishonest misappropriation of property of deceased person(3 yrs or 7, if employed as clerk/servant)
* section 420 - cheating and dishonestly inducing delivery of property(7 yrs)
* or any offence punishable by imprisonment of under 2 years or with fine or both

Conditions:
1. no previous conviction is proved against him
2. opinion based on circumstances
  a. nature of the offence and
  b. the character of the offender

### Probation

Conditions:
1.  an offence not punishable with death or imprisonment for life
2. opinion based on circumstances
  a. nature of the offence and
  b. the character of the offender


3 years, peace and good behavior based on probation officer's report

### probation officer

Appointed by SG or recognised society by SG or exceptional cases as per court's choice pertaining to special circumstances.

Appointed by court of that case or District Magistrate to replace the one in order.







-----
