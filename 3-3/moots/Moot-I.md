
# Deliverables

## Written record
1. Problem what sir has given
2. Legal notice
3. Plaint
4. Affidavit
5. Written statement

## Syllabus

brief summary of facts, issues involved, provisions of laws and arguments, citation, prayer
Marks for oral advocacy may be awarded for communication skills, presentations, language, provisions of law; authorities quoted, court manners, etc.
Written Memorials submitted by the students shall be kept by the College for Further Verification.




## Written Record

### Civil Case - Problem
Mr. Sundaram is a private financier having his office at Yousufguda. He lends
money to small business owners in the surrounding areas such as owners of
provision stores, fruit juice vendors, stationery shop owners etc., He charges
interest @ 3% (i.e., Rs 3/- per hundred per month) on the amount lent and he
always insists on empty promissory note on which a



Setting:

Loan Taken: 01-01-2018
Loan Due: 01-01-2020
Rash reply: 04-02-2020
Legal Notice Date: 05-02-2020
Suit Filed: 09-03-2020
Written Statement 12-03-2020




                              LEGAL NOTICE
                              ------------

G V Aditya
Advocate
H No. 1-2-3,
Kondapur,
Hyderabad-500084.

BY COURIER/SPEED POST/EMAIL   
11th February 2020




To:
Mr. Veera Bhadram,
H No. 4-5-6,
Yellareddyguda,
Hyderabad-500073, India.


Reg: Repayment of hand loan received from Mr. Sundaram on 01-01-2018

Dear Sir,

Under instruction and on behalf of my client Mr. Sundaram, S/o Raghava Ram, aged 40, a private financier, Resident of Yousufguda, I hereby serve you with the following notice:-

1. That my client Mr. Sundaram is a private financier having business at
Yousufguda main road. His regular business is to lend money to the needy
businessmen who normally fall short of working capital in their running business.
2. That on 01 Jan 2018, my client, out of friendship, had lent you Rs 5,00,000/- as a short term finance with an interest at the rate of 3% per month. And that on the same day, you have executed a promissory note dated 01 Jan 2018, and promised to pay the amount along with interest by 01 Jan 2020.
3. That it is agreed upon in the promissory note that you would make the payments of Rs 15,000 every month towards interest for 24 months, until the due date, at which time the principal amount would be repaid to my client in full.
4. That after making interest payments for 18 months, you missed payment on 1st Aug 2019 and despite reminders from my client have only made one more interest payment on 1st September 2019 and 1st November 2019.
5. That you had requested my client for time to make the payments at the time and my client had agreed to the same.
6. That on the due date, viz. 1st Jan 2020, my client called you up on phone to pay back the loan along with the entire interest amount due. And that you expressed inability to pay the money and requested my client for a month's time.
7. That on 04 Feb 2020, i.e, after one month period, my client even went to your place to collect the due amount and that you gave a rash reply to him saying that you cannot pay the debt due and asked my client to do whatever he wanted in this regard.
8. That you are liable to pay the total amount of Rs 569,872 to my client including interest due and he is entitled to recover the same from you.


Under these circumstances, you are called upon to make the payment of Rs 569,872/- (Rupees five lakhs sixty nine thousand eight hundred and seventy two only) to my client within 30 days from the date of receipt of this notice failing which, as instructed by my client, necessary legal action will be taken against you and you shall be liable for all the costs and consequences resulting therefrom.

You are also liable to pay a sum of Rs 5000 towards charges for this legal notice.

Kindly take the notice.

A copy of this notice is kept intact in my office for record and further necessary action and you are also advised to keep the copy of the same as safe as you would be asked to produce in the court.


Thanking you,

Sd/-
Advocate

                                                        Place:
                                                        Date:



Principal:     500,000
Aug interest:   15,000
Sept due:      530,450 - 15,000 paid
Sept net due:  515,450
Oct due:       530,913.50
Nov due:       546,840.905 - 15,000 paid
Dec due:       531,840.905
Jan due:       547,796.13215
Feb due: 564230.0161145
Current day due: 569872.316275645



IN THE HONORABLE COURT OF JUNIOR CIVIL JUDGE, CITY CIVIL COURT, HYDERABAD

                  Summary Suit No. 1987


BETWEEN
Mr. Sundaram,
S/o Raghava Ram,
 aged 40 years,
a private financier, residing at 1-2-3, Yousufguda, Hyderabad ............ Plaintiff


AND

Mr. Veera Bhadram,
S/o Pedda Bhadram,
aged 55 years,
a grocer, residing at 4-5-6, Yellareddyguda, Hyderabad
 ....................   Defendant


                SUIT FILED FOR RECOVERY OF MONEY

PLAINT FILED UNDER ORDER VII, Rule 1 and Rule 2 READ WITH SECTION 26 of CIVIL PROCEDURE CODE

1. Description of the Plaintiff:
The plaintiff Mr. Sundaram, aged 40 years, occupation private financier, having office and residence in Yousufguda and address mentioned above cause list for all further communication.
2. Description of the Defendant:
The defendant Mr. Veera Bhadram, aged about 55 years, occupation owner of a grocery shop at Yellareddyguda and the address mentioned above will be used for serving of summons etc by the court.

Brief Details of the case:
3. That the plaintiff is a private financier having business at
Yousufguda main road. His regular business is to lend money to the needy
businessmen who normally fall short of working capital in their running business.
4. That the plaintiff charges 3% interest per month from borrowers as a standard rate.
5. That the defendant Mr. Veera Bhadram approached the plaintiff for a hand loan of Rs 5,00,000/-(Rupees five lakhs only) for meeting his working capital needs.
6. That the plaintiff and defendant were known to each other and that the plaintiff agreed to lend the amount to the defendant based on their friendship. After executing the promissory note, the plaintiff transferred the said amount to the defendant's bank account.
7. It was agreed between the plaintiff and the defendant that the said amount along with interest will be redeemed by the plaintiff immediately at the end fo the second year from the date of borrowing and the interest amount will have to be paid by the defendant promptly on a monthly basis to the plaintiff.
8. As per the agreed terms, the the defendant had paid interest promptly for 18 months but thereon failed to pay the interest regularly accumulating the interest amount.
9. That the plaintiff had called upon the defendant regarding the loan amount and remaining interest payment, but the defendant failed and requested for some more time. Further that the defendant started avoiding calls from the plaintiff.
10. That the plaintiff even went to the place of defendant on 04 Feb 2020 to collect the sum due, but the defendant gave a rash reply to the plaintiff, suggesting that the plaintiff do whatever he can but that he would not get his money back.
11. Cause of Action: That the cause of action arose on 04 Feb 2020. The plaintiff and defendant both reside in Hyderabad. The promissory note was executed in Hyderabad as well.
12. Jurisdiction: That the transaction took place in Hyderabad and the claim amount is Rs 569872/-(Rupees five lakhs sixty nine thousand eight hundred and seventy two only). The pecuniary limit of this Honorable Court is Rs 20,00,000 as per The Telangana Civil Courts Amendment Act 2019.
13. Limitation: Cause of action is within limitation period.
14. Valuation: That the valuation for the purpose of the suit is as under Telangana Court Fees and Suits Valuation Act 1956.









Pecuniary Jurisdiction:
THE TELANGANA CIVIL COURTS ACT, 1972.
To increase pecuniary jurisdiction from 3 lakhs to 20 lakhs for JCJ
AP amended in 2018
TS amended in 2019
Section 5(3)




1. Particulars to be contained in plaint— The plaint shall contain the following particulars:—
(a) the name of the Court in which the suit is brought ;
(b) the name, description and place of residence of the plaintiff;
(c) the name, description and place of residence of the defendant, so far as they can be ascertained;
(d) where the plaintiff or the defendant is a minor or a person of unsound mind, a statement to that effect; (e) the facts constituting the cause of action and when it arose;
(f) the facts showing that the Court has jurisdiction; (g) the relief which the plaintiff claims;
(h) where the plaintiff has allowed a set-off or relinquished a portion of his claim, the amount so allowed or relinquished; and
(i) a statement of the value of the subject-matter of the suit for the purposes of jurisdiction and of court fees, so far as the case admits.
2. In money suits— Where the plaintiff seeks the recovery of money, the plaint shall state the precise amount claimed:



Limitation Act 1963 - 6 years














----
