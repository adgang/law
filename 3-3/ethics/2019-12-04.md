


An advocate shall not do anything whereby he abuses or takes advantage of the confidence reposed in him by his client.


An advocate should keep accounts of the client's money.

Rule 26:

Divert money from accounts.

Rule 27: Receipt must be intimated to the client on amounts.

After termination of proceedings, the advocate has adjust the

Rule 30: Copy of account of the client.


Rule 31: An advocate shall not enter into arrangements whereby funds in his hands are converted into loans.

Rule 32: An advocate shall not lend money to his client.

An advocate shall not be held guilty in breach of this rule.


Rule 33: No appear or plead for the opposing party.

Rule 34:
Duty to opponent:
1. Not to communicate or negotiate upon the subject matter of controversy with opposing party.


Rule 35:

Carry out legitimate promises made to the opposing party.

Rule 36:
Duty to colleagues
Not to advertise or solicit work


Name
Phone
Qualification
Address

Signboard or Nameplate should be of a reasonable size:

Rule 37:
Not promote unauthorized practice of law.


Rule 38:

An advocate cannot charge a fee less than taxable under the rules when the client is able to pay the same.


Rule 39:
Consent of fellow advocates to appear.

Rule 40-44:


Rule 45:
Duty in imparting training

Improper to demand or accept fees or any premium from any person as a consideration.

Rule 46:
Duty to render legal aid
Free legal assistance to indigents can be given

Within the limits of an advocate's economic condition, free legal assistance to the indigent and oppressed is one of the highest obligations an advocate owes to society.












----
