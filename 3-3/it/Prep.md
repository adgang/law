# Syllabus

Unit-I:
Concept of Information Technology and Cyber Space- Interface of Technology and Law - Jurisdiction in Cyber Space and Jurisdiction in traditional sense - Internet Jurisdiction - Indian Context of Jurisdiction -Enforcement agencies - International position of Internet Jurisdiction - Cases in Cyber Jurisdiction
Unit-II:
Information Technology Act, 2000 - Aims and Objects — Overview of the Act – Jurisdiction –Electronic Governance – Legal Recognition of Electronic Records and Electronic Evidence -Digital Signature Certificates - Securing Electronic records and secure digital signatures - Duties of Subscribers - Role of Certifying Authorities - Regulators under the Act -The Cyber Regulations Appellate Tribunal - Internet Service Providers and their Liability– Powers of Police under the Act – Impact of the Act on other Laws .
Unit-III:
E-Commerce - UNCITRAL Model - Legal aspects of E-Commerce - Digital Signatures - Technical and Legal issues - E-Commerce, Trends and Prospects - E- taxation, E-banking, online publishing and online credit card payment - Employment Contracts - Contractor Agreements, Sales, Re-Seller and Distributor Agreements, Non- Disclosure Agreements- Shrink Wrap Contract ,Source Code, Escrow Agreements etc.
Unit-IV:
Cyber Law and IPRs-Understanding Copyright in Information Technology - Software – Copyrights vs. Patents debate - Authorship and Assignment Issues - Copyright in Internet - Multimedia and Copyright issues - Software Piracy – Patents - Understanding Patents - European Position on Computer related
Patents - Legal position of U.S. on Computer related Patents - Indian Position on Computer related Patents –Trademarks :Trademarks in Internet - Domain name registration - Domain Name Disputes & WIPO -Databases in Information Technology - Protection of databases - Position in USA,EU and India
Unit-V:
Cyber Crimes -Meaning of Cyber Crimes –Different Kinds of Cyber crimes – Cyber crimes under IPC, Cr.P.C and Indian Evidence Law - Cyber crimes under the Information Technology Act, 2000 - Cyber crimes under International Law – Hacking, Child Pornography, Cyber Stalking, Denial of service Attack, Virus Dissemination, Software Piracy, Internet Relay Chat (IRC) Crime, Credit Card Fraud, Net Extortion, Phishing etc - Cyber Terrorism - Violation of Privacy on Internet - Data Protection and Privacy.


* Name of Act
* Year
* Object of Act
* Functionaries


# All Acts and Objects

1. Information Technology Act, 2000 - ACT NO. 21 OF 2000
An Act to provide legal recognition for transactions carried out by means of electronic data interchange and other means of electronic communication, commonly referred to as ―electronic commerce‖, which involve the use of alternatives to paper-based methods of communication and storage of information, to facilitate electronic filing of documents with the Government agencies and further to amend the Indian Penal Code, the Indian Evidence Act, 1872, the Banker’s Books Evidence Act, 1891 and the Reserve Bank of India Act, 1934 and for matters connected therewith or incidental thereto.
WHEREAS the General Assembly of the United Nations by resolution A/RES/51/162, dated the 30th January, 1997 has adopted the Model Law on Electronic Commerce adopted by the United Nations Commission on International Trade Law;
AND WHEREAS the said resolution recommends inter alia, that all States give favourable consideration to the said Model Law when they enact or revise their laws, in view of the need for uniformity of the law applicable to alternatives to paper-based methods of communication and storage of information;
AND WHEREAS it is considered necessary to give effect to the said resolution and to promote efficient delivery of Government services by means of reliable electronic records.
2. UNCITRAL Model Law on Electronic Commerce (1996)
The purpose of the Model Law is to offer national legislators a set of internationally acceptable rules as to how a number of such legal obstacles may be removed, and how a more secure legal environment may be created for what has become known as “electronic commerce”
The principles expressed in the Model Law are also intended to be of use to individual users of electronic commerce in the drafting of some of the contractual solutions that might be needed to overcome the legal obstacles to the increased use of electronic commerce.
The Model Law may be useful in certain cases as a tool for interpreting existing international conventions and other international instruments that create legal obstacles to the use of electronic commerce, for example by prescribing that certain documents or contractual clauses be made in written form.
The objectives of the Model Law, which include enabling or facilitating the use of electronic commerce and providing equal treatment to users of paper-based documentation and to users of computerbased information, are essential for fostering economy and efficiency in international trade. An enacting State would create a media-neutral environment.
3. THE INDIAN PENAL CODE, 1860 - ACT NO. 45 OF 1860
WHEREAS it is expedient to provide a general Penal Code for India
4. THE CODE OF CRIMINAL PROCEDURE, 1973 - ACT NO. 2 OF 1974
An Act to consolidate and amend the law relating to Criminal Procedure.
5. THE INDIAN EVIDENCE ACT, 1872 - ACT NO. 1 OF 1872
WHEREAS it is expedient to consolidate, define and amend the law of Evidence
6. THE BANKERS’ BOOKS EVIDENCE ACT, 1891 - ACT NO. 18 OF 1891
WHEREAS it is expedient to amend the Law of Evidence with respect to Bankers’ books


# Functionaries/Authorities

(c) ―adjudicating officer means an adjudicating officer appointed under sub-section (1) of section 46;
(da) ―Appellate Tribunal means the Appellate Tribunal referred to in sub-section (1) of section 48;
(g) ―Certifying Authority means a person who has been granted a licence to issue a 1[electronic signature] Certificate under section 24;
(m) ―Controller means the Controller of Certifying Authorities appointed under sub-section (1) of section 17
(ua) Indian Computer Emergency Response Team‖ means an agency established under sub- section (1) of Section 70B;

* Controller of Certifying Authorities - Deputy Controllers, Assistant Controllers, other officers and employees
* Certifying Authority - can be foreign
* adjudicating officer for holding an inquiry - CG appointed officer not below the rank of a Director to the Government of India or an equivalent officer of a State Government
* APPELLATE TRIBUNAL - The Telecom Disputes Settlement and Appellate Tribunal established under section 14 of the Telecom Regulatory Authority of India Act, 1997
* 70B Indian Computer Emergency Response Team
Director General and such other officers
* 78. Power to investigate offences.–Notwithstanding anything contained in the Code of Criminal Procedure, 1973 a police officer not below the rank of 1[Inspector] shall investigate any offence under this Act.
* Examiner of Electronic Evidence - providing expert opinion on electronic form evidence before any court or other authority, CG by notification in the Official Gazette, any Department, body or agency of the Central Government or a State Government as an Examiner of Electronic Evidence.
* power to search - any police officer, not below the rank of a Inspector or any other officer of the Central Government or a State Government authorised by the Central Government in this behalf
* Cyber Regulations Advisory Committee - a Chairperson and such number of other official and non-official members representing the interests principally affected or having special knowledge of the subject-matter as the Central Government may deem fit.
to advise CG as regards any rules or for any other purpose connected with this Act; to advise the Controller in framing the regulations under this Act.


adjudicating officer or controller
->
Appellate Tribunal (within 45 days, with 45 days leeway, decision within 6 months)
->
HC (within 60 days, with 60 days leeway) - on any question of fact or law arising out of such order



# Topics

##

PRELIMINARY - Section 1-2
DIGITAL SIGNATURE AND ELECTRONIC SIGNATURE - Section 3
E-governance - Sections 4-10A
ATTRIBUTION, ACKNOWLEDGEMENT AND DESPATCH OF ELECTRONIC RECORDS - Section 11-13
SECURE ELECTRONIC RECORDS AND SECURE ELECTRONIC SIGNATURE - Section 14-16
REGULATION OF CERTIFYING AUTHORITIES - Section 17-34
ELECTRONIC SIGNATURE CERTIFICATES - Section 35-39
DUTIES OF SUBSCRIBERS - Section 40-42
PENALTIES, COMPENSATION AND ADJUDICATION - 43-47
THE APPELLATE TRIBUNAL - 48-64
OFFENCES - 65-78
INTERMEDIARIES NOT TO BE LIABLE IN CERTAIN CASES - 79
EXAMINER OF ELECTRONIC EVIDENCE - 79A
MISCELLANEOUS - 80-90

2 schedules

1st schedule - NI except cheque, trust, will, power of attorney, sale or conveyance of immovable properties

IInd schedule Aadhar e-KYC and all other e-KYC 




-----
