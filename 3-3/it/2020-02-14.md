Fri 14 Feb 2020 11:33:30 AM
-------

### Creating a digital signature



Msg -> Applies Hash Function -> Gets Hash Result/Message Digest



Encrypt (Message + Message Digest) with private key -> Checksum is Digital Signature

### Verifying Digital Signature


Apply public key on Digital Signature -> Recovered Hash Result/Message Digest
from Digital Signature -> Computes new Hash Result/MD by original HF -> Compare both Hash results




I can give a session on Digital Signature for interested ppl covering the following:
0. What are the essentials of a signature?
1. What is the different between normal signature, electronic signature and digital signature?
2. How does electronic signature ensure forgery cannot happen? Why can't people just copy signature just like they copy files?
3. How to create your own electronic signature scheme?
4. How to sign documents with your own electronic signature?
5. Why you should not use your own electronic signature scheme(why is it not strong) but use the ones developed by experts(also called digital signature)?
6. Can a digital signature 'expire'?

Optional topics:
1. How to know what a person's signature looks like(to verify signature)?
2. What if the source of such verification(repository of public keys) is hacked?
3. How to apply for a digital signature?
4. What is difference between Symmetric and Asymmetric key encryption?
5. Blockchain




The basic problem with digital signature regime is that it operates in online
that is a software driven space.

The problem comes in relation with the authenticity and non-repudiation.

For example, A sends to B a digitally signed message, how would B make sure that
it is a message indeed originated from A.

How to authenticate that the message was from A only.


This calls for the participation of trusted third parties for individual's identity.


This trusted third party(TTP) is the certifying authority who issues the digital signature certificate.

Subscriber is a person who applies for digital signature certificate.


Certifying authority forwards the certificate to the repository maintained by
the controller.


Controller of Certifying Authority


Subscriber can use the digital signature.

The verifying party(receiver of the document which is digitally signed) will verify the digital signature with the public key and will check the status and validity of certificate in the repository maintained by the controller of certifying authority.



## Electronic Signature

Section 3(A) -

The electronic signature provides for technology neutrality which means that the law will remain neutral vis-a-vis adoption of technology as a user would have a
choice to adopt or use any electronic signature.

Examples: PIN code, biometric identification, voice recognition, fingerprint,
retina scan etc.


e-signature includes digital signature.

The encryption techniques whatever being used to validate the authenticity of the person will come under the electronic signatures.

Electronic evidence

It is any probative information stored or transmitted digitally and a party to the dispute in the court can use the same during the trial.

Court permits the use of digital evidence such as e-mails, digital photographs, word documents, instant message histories, internet browsing histories, databases, contents of computer memory, computer backup, secured e-records and secured digital signatures.


Section 79(A) - Electronic form evidence means any information of probative value that is either stored or transmitted in e-form and includes the computer evidence, digital video, audio, cellphones, digital fax machines.

Examiner of e-evidence: 












--------
