Thu 30 Jan 2020 10:24:30 AM
----

## Jurisdiction in Cyber Law

Cyberspace is a case where messages and webpages are posted for the reference and retrieval in any part of the world.

The legal metaphor of cyberspace will be outside national boundaries.

Cyberspace is located in no particular geographical location but is available to anyone from anywhere just through the access to the internet.

In the current stage, the term cyberspace stands for the global network of interdependent information technology from infrastructures, telecommunication networks and computer processing systems.

In international law, there exists a type of territory called as international space. Currently there are three such international spaces.

1. Antarctica
2. Outerspace
3. High seas

For jurisdictional analysis, cyberspace should be treated as fourth international space

The above three are of physicality in nature and the last is of non-physical nature.

In cyberspace, jurisdiction is the overriding conceptual problem for domestic and foreign countries alike unless it is conceived as an international space.

Jurisdiction in cyberspace requires clear principles rooted in international law.

Only through these principles can the courts in all nations be persuaded to adopt uniform solutions to the problems of cyberspace jurisdiction.

## Principles of Jurisdiction

1. Subjective territoriality
2. Objective territoriality
3. Nationality
4. Protective principle
5. Universal Jurisdiction

### Subjective territoriality
If a particular act or conduct is committed within the boundaries of the regulating state, then such state shall be entitled to lay down the law that would govern such an act or conduct.

### Objective territoriality
If a particular act in question takes place in another territory, however the effect of the act directly or indirectly is substantially felt within the forum state, commonly known as the effects jurisdiction.

This principle has widely emerged into the accepted list of cyberspace.

### Nationality

A national law is territory specific and nationality is the basis of jurisdiction where the forum state asserts the right to prescribe a law for an action based on the nationality of the person.

Nationality may be acquired by:
1. by birth
2. by naturalisation
3. by resumption
4. by subjection
5. by cession
6. by migration

### Protective principle

SI Madankumar of cybercrime


### Protective principle
The protective principle expresses the desire of a sovereign to punish the actions committed in other places solely because it feels threatened by those actions. The protective principle finds the application where a country takes necessary action in a foreign state to secure its national integrity or security.







----
