
# Syllabus
## Unit - I - History, Suits, Jurisdiction
CPC, history, Suits, what, how, cause of action, jurisdiction, Summons  
## Unit - II - Pleadings, Plaint, WS, Framing of Issues
Pleadings, contents, how, types, amendments, Plaint, essentials, return, rejection, documents, written statements, counter-claim, setoff, Sec 89(arbitration), Framing of issues  
## Unit - III - Hearing, Judgement, Execution
Appearance, Ex-parte, Witnesses and summons, Hearing, Judgement, orders, injunctions, Receivers and Commissions, Costs, Execution,
## Unit - IV - Special suits, Incidental proceedings, Appeals, Reference, Review
Special suits, Incidental proceedings, Appeals, Reference, Review
## Unit - V - Limitation, Computation, Legal Disability



# Acts
1. Civil Procedure Code, 1908
2. Limitation Act, 1963

Excl Arbitration Act, 1940


# Definitions

## Code
A systematic collection of statutes, body of laws so arranged as to avoid inconsistency and overlap


## Letters Patent
A document conferring a patent or other right by the sovereign.



## Judge
means the presiding officer of a Civil Court


## Foreign Court
means a Court situate outside India and not established or continued by the authority of the Central Government;

## Decree
A formal expression
of adjudication
which as far as regard this Court
conclusively determines the rights of parties
regard to all or any of matter in controversy in suit

Includes:
1. Rejection of plaint
2. questions within Section 144 (costs, execution costs etc)

Excludes:
1. any adjudication which leads to an appeal from an order
2. any order for dismissal for default



## Order
inconclusively determines a right


## Rules - Section 2(18)
rules and forms in First Schedule and rules under Section 122 or Section 125

# Unit - I

## History
First Civil Procedure Code - 1859
Did not apply to Supreme Courts and Sadar Diwani Adalats

In 1861, High Courts Act made the code apply to all courts.

Second CPC - 1877
Third CPC - 1882
Final CPC - 1908

In force Jan 1, 1909




158 sections - 12 parts

Schedule - Rules and Orders


## Section 121-131 - Rules

Rule Committee:
Appointed by HC, with one of them as President:
1. 3 HC judges
2. 2 lawyers
3. one Judge of Civil court subordinate to HC

Membership as decided by HC or until death, retirement, dies or stops living in the State

A secretary will be appointed by SC

Section 140 - assessors in salvage

Section 142 - orders and notices to be in writing

## Section 132 - 158 - Misc

### Section 151 - inherent powers of court


### Section 152 - Amendments of judgements, decrees or orders - clerical or arithmetic arising from accidental slip or omission - can be correct by court on own or on application of parties.


### Section 153 B - open court

154, 155, 156 - repealed

157 - saving all notifications, declarations, rules same force as far as consistent with CPC

158 - references to old code is to this code afap

Section 10 - Res subjudice



Section 11 - Res judicata

### Res judicata

No court shall try:
any suit or issue in which
the matter directly and substantially in issue
has been directly and substantially in issue
in a former suit
between the same parties
or between parties under whom they or any of them claim,
litigating under the same title
in a Court competent to try such subsequent suit
or the suit in which such issue has been subsequently raised
and has been heard finally decided by such Court


E1. former suit - the suit decided first, whether instituted first or not
E2. competence - irrespective of any provisions as to right of appeal from decision of such Court
E3. matter must have been alleged by one party in former suit and the other admitted/denied it explicitly/implicitly
E4. any matter which might/ought to have been ground for attack is deemed have been a matter directly and substantially in issue in such suit
E5. any relief claimed in plaint, not expressly granted is deemed rejected
E6. when former suit is litigating public right or private right in common, all persons interested shall be deemed to claim under persons so litigating
E7. execution of decree
E8. an issue decided by court of limited jurisdiction will operate as res judicata in a subsequent suit notwithstanding the Court not being competent to try that suit


Constructive Res judicata

is a subset of the doctrine of res judicata. It is in Section 11 Explanation IV.

It sets a bar on claims in a suit which should/ought to have been raised and decided upon in a formed suit.

A signs a pronote with B. B files for recovery. A claims that B coerced the signature. This claim was rejected.

In a subsequent suit, A cannot challenge this pronote on a new claim that B committed fraud.


### Section 12

A bar from rules bars in all courts where this Code applies.

### Section 13 - When is foreign judgement not conclusive?

(a) no jurisdiction  
(b) not on merits  
(c) incorrect view of international law or refusal to recognize Indian law in cases where Indian law is applicable  
(d) proceedings to judgement are opposed to natural justice  
(e) obtained by fraud  
(f) sustains a claim founded on a breach of Indian law(any law in force in India)



### Section 15 - Court in which suits to be instituted
Every suit shall be instituted in the Court of the lowest grade competent to try it.






# Limitation Act 1963

32 sections
and a schedule



----
