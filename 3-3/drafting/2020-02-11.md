Tue 11 Feb 2020 10:36:16 AM
----





consenting to get a divorce fairly and smoothly without any scope for contention between the parties about alimony, child custody, maintenance, property and so on.

After the trial, if the court is convinced, it passes a decree of divorce making divorce final.


Recent judgements of courts

125 CrPC and maintenance




## Divorce Petition

IN THE COURT OF HONOURABLE FAMILY COURT JUDGE, SANGAREDDI
-------------

HM OP NO: 129 of 2020



Between
A, D/o P, Aged xy, occupation plumber, R/o xyz ................... Petitioner 1

and

B, S/o Q, Aged yy, occupation househusband, R/o xyz .............. Petitioner 2



PETITION FOR DISSOLUTION OF MARRIAGE BY DECREE OF DIVORCE BY MUTUAL CONSENT
---------------------------------
UNDER SECTION 13B of HINDU MARRIAGE ACT 1955
-------------------






That the petitioners no.1 and no.2 most respectfully submits as follows:

1. That the marriage was solemnised between petition no. 1 and petitioner no.2
according to Hindu rites and marriage performed on xx/xx/xxxx at Sri Venkateswara
Swamy Temple, Kushaiguda, Hyderabad without any dowry given by petitioner no. 1.
The marriage was an arranged marriage and solemnized in front of relatives,
friends and well-wishers on both sides.
2. That the petitioner no.1 is the legally wedded second wife of petitioner no. 2.
And petitioner no. 1 is well aware of earlier marriage of petitioner no. 2
3. That immediately after the marriage petitioners no.1 and no.2 started staying at
petitioner no.2's residence at BHEL of Telangana state. Though both the petitioners
 stayed together for two years, they never led a happy married life and they were
 not blessed with any children.
4. That after two years of their marriage, sharp differences arose between petitioner no. 1
and petitioner no. 2 and as a result, they have been living separately and have
not been able to live together since then.
5. That the well-wishers of the petitioners tried and advised them to sort out
the differences and live together but petitioners could not reconcile and there
has been no resumption of conjugal rights between the petitioners. And since then,
petitioner no.1 has been staying at her parents' house.
6. That since there is absolutely no scope for the petitioners to live together
and lead a matrimonial life in future, petitioner no.1 and petitioner no. 2 have
mutually agreed that their marriage should be dissolved.

That the mutual consent has not been obtained by force, fraud or undue influence
and also petition is not presented in collusion

That both the petitioners are ready to relinquish all rights on each others'
property where the petitioner no. 1 shall not claim any maintenance from petitioner no. 2
 and petitioner no. 2 as well as any property of petitioner no.1
and petitioner no. 2 shall not claim any of the property of petitioner no. 1

That there are no proceedings between the petitioners before any other court of
law in respect of their marriage.

Cause of Action: The cause of action for filing the petition arose on date xx/xx/xxxx
when the marriage of petitioner 1 and petitioner 2 was performed and when the sharp
differences arose between petitioner 1 and petitioner 2 and both have been staying away
from each other since then.




Abbreviations:

HMOP - Hindu Marriage Original Petition  



---
