Wed 19 Feb 2020 12:24:39 PM
------

Types of listing:
1. Initial listing - securities listed for the first time by a company on a stock exchange is called initial listing
2. Listing for public issue - Shares listed on a stock exchange comes out with a public issue of securities and it has to list such securities
3. Listing for rights issue - Securities listed on a stock exchange to existing shareholders on rights basis is called listing for rights issue.
4. Listing for mergers - where new shares are issued by an amalgamated company to the shareholders of amalgamating company. Such shares are also required to be listed on the concerned stock exchange.


## Section 21 A -Delisting of securities:
1. A recognized stock exchange may delist the securities after recording the reasons on any of the grounds as may be specified provided a reasonable opportunity is given to the concerned company.
2. If a company fails to comply with the listing agreement, as a consequence, stock exchange may delist the securities.
3. The term delisting of securities means permanent removal of securities of a listed company from a stock exchange as a result of which company would no longer be traded at that stock exchange.
4. Delisting may be compulsory delisting or voluntary delisting.


### Compulsory delisting
It refers to permanent removal of securities of a listed company from stock exchange as a penalising measure for not submitting/complaint with various requirements set out in the listing agreement within the time prescribed.

### Voluntary delisting
A listed company on its own decides to permanently remove its securities from a stock exchange, a company may delist its equity shares from all or from the only RSE where they are registered and in case of conversion of company, that is, from public to private, and in case of mergers, so on.

A listed company, or an aggrieved party, may file an appeal before the SAT(Securities Appellate Tribunal) against the decision of RSE delisting the securities within *15 days* from the date of decision passed by RSE.

If the applicant has shown sufficient reasons for not filing it within 15 days, and the presiding officer of SAT is satisfied with the reasons given by party, then he may condone the delay for another 30 days.

### Section 22 - Right of appeal against refusal of stock exchange to list securities


### Section 23 - Penalties

Any person who without any reasonable excuse fails to comply with any requisitions, such as submission of periodical returns or enters into a contract in contravention of sections 13 or 16 or 17 or 19 or 18A shall be punished with imprisonment which may extend to 10 years or with fine which may extend upto to 25cr or both liable to play a penalty *not exceeding Rs 1 cr*.

### Section 23 A -
Penalty for failure to furnish information, returns etc.


Any person who is required under the act to furnish any information, documents, books, returns, reports to RSE and fails to furnish such information shall be liable to pay penalty not less than one lakh per day till the default continues or Rs 1cr whichever is less.

### Section 23 B - Penalty for failure by any person to enter into an agreement with client


If any person who is required under this Act to enter into an agreement with his client and fails to enter into such agreement, he shall be liable to pay a penalty(same as above).



MOU or Cover Note - Agreement between TM(Trading Members) and RSE


### Section 23(c) - penalty to redress investors' grievances

If any stock broker or a sub-broker or a company whose securities are listed and after being called up on by SEBI in writing to redress the grievances of investor and fails to redress such grievances is liable to pay the penalty(same as above).







-------
