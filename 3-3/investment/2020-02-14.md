Fri 14 Feb 2020 12:27:41 PM
-----




Section 8(A) Clearing Corporation


CH -> Clearing Corporation


registered under CA, MOA, AOA,

CC should be under SEBI


Clearing House duties:
Day to day transactions, settlement of transactions



Recognized Stock Exchange(RSE) with the prior approval of SEBI may transfer the duties and functions of Clearing House to Clearing Corporation, being a company incorporated under the Companies Act for the purposes of -
1. periodical settlement of contracts and differences thereunder
2. delivery of securities and payment of securities
3. other matters connected with transfer

Every clearing corporate for the purpose of transfer of duties and functions of a clearing house to clearing corporation shall make by-laws and submit the same to SEBI for its approval if SEBI is satisfied may grant the approval for transfer of duties and functions from Clearing House to Clearing Corporation.


### Section 9 - Power of RSE to make by-laws
Any RSE subject to prior approval of SEBI make bylaws for the regulation and control of contracts.

### Bylaws

1. Opening and closing of markets
2. Regulation of hours of trade
3. A Clearing House for the periodical settlement of contracts and differences thereunder
4. Submission to SEBI by Clearing House all the periodical settlements
5. Regulation or prohibition of carryover facilities


Acts -> Rules -> Regulations -> Notifications -> Circulars

### Section 10 - Power of SEBI to make or amend by-laws of RSE

SEBI may either on its own, or on request in writing received by it by the governing body of RSE shall make bylaws for all or any of the matters specified in Section 9 or amend the by-laws made by such stock exchange under Section 9.


Any by-laws made or amended by board shall be published in the gazette of India and also in the official gazette of state where such RSE is situated and upon publication, they shall come into effect.


where the governing body of Recognized Stock Exchange objects any bylaws made or amended by SEBI, it may within 2 months of publication in the gazette of India apply to SEBI for revision thereof and SEBI, after giving an opportunity of being heard shall revise the bylaws so made or amended.

So amended bylaws shall be published in the respective gazettes and they shall become effective.

### Section 11 - Power of CG to supersede governing body of RSE

CG/SEBI



From 1992 SEBI is in control of all RSEs. Prior to that CG was in control.



When SE is not functioning according to SCRA Act, prejudicial to clients

Where CG is of the opinion that the governing body(GB) of the RSE should be superseded, may serve a written notice on the GB of RSE specifying the reasons and also an opportunity of being heard in the matter. CG by notification in the official gazette declare that the governing body of RSE to be superseded and may appoint any person or persons to exercise and perform all the powers and duties of GB.


Persons appointed as GB shall hold office for such period as specified in the notification or till the publication of further notification by CG.

CG may at any time before the completion of period of office of any person/persons appointed as GB may terminate their services and ask the GB to resume their office at the SE. Or it may ask the SE to have a fresh body of governing members.


### Section 12 - Power of CG to suspend business of RSE

If in the opinion of CG, an emergency has arisen and for the purpose of meeting the emergency CG considers it to suspend the business of any stock exchange or all stock exchanges provided that period should not exceed seven days subject to such conditions as may be notified.

The period may be extended by serving further notifications from time to time.


Questions:

Listing and delisting
Title to Dividends
Salient features of SCRA Act


-----
