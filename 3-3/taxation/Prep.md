# Syllabus


Unit-I:
LL.B.VI SEMESTER
PAPER-I:
LAW OF TAXATION
Constitutional basis of power of taxation — Article 265 of Constitution of India - Basic concept of Income Tax — Outlines of Income Tax Law - Definition of Income and Agricultural Income under Income Tax Act — Residential Status - Previous Year — Assessment Year — Computation of Income.
Unit-II:
Heads of Income and Computation — Income from Salary, Income from House Property. Profits and Gains of Business or Profession, Capital Gains and Income from other sources.
Unit-III:
Law and Procedure — P.A.N. — Filing of Returns — Payment of Advance Tax -- Deduction of Tax at Source (TDS) -- Double Tax Relief — Law and Procedure for Assessment, Penalties, Prosecution, Appeals and Grievances -- Authorities.
Unit-IV:
GST ACT, 2017 – Goods and Services Tax Act, 2017: Introduction – Background - - Basic Concepts – salient features of the Act – Kinds of GST - CGST, SGST & IGST – Administration officers under this Act – Levy and collection of tax – scope of supply – Tax liability on composite and mixed supplies – Input tax credit – Eligibility and conditions for taking input tax credit.
Unit-V:
GST ACT, 2017:- Registration – persons liable for registration – persons not liable for registration – procedure for registration – returns – furnishing details of outward and inward supplies – furnishing of returns – payment of tax, interest, penalty and other amounts – tax deducted at source – collection of tax at source – Demand and Recovery – Advance Ruling – Definitions for Advance Ruling – Appeals and revision – Appeals to Appellate Authority – Powers of revisional authority - Constitution of Appellate Tribunal and benches thereof – offences and penalties.



* Name of Act
* Year
* Object of Act
* Functionaries


# All Acts and Objects

1. The Income Tax Act, 1961 - (43 of 1961)
An Act to consolidate and amend the law relating to income-tax [and super-tax].
2. The Constitution (One Hundred and First Amendment) Act, 2016 - 8th September, 2016
An Act further to amend the Constitution of India.
3. THE CENTRAL GOODS AND SERVICES TAX ACT, 2017 - ACT NO. 12 OF 2017
An Act to make a provision for levy and collection of tax on intra-State supply of goods or services or
both by the Central Government and for matters connected therewith or incidental thereto.
4. THE INTEGRATED GOODS AND SERVICES TAX ACT, 2017 - ACT NO. 13 OF 2017
An Act to make a provision for levy and collection of tax on inter-State supply of goods or services or both by the Central Government and for matters connected therewith or incidental thereto.
5. THE UNION TERRITORY GOODS AND SERVICES TAX ACT, 2017 - ACT NO. 14 OF 2017
An Act to make a provision for levy and collection of tax on intra-State supply of goods or services or
both by the Union territories and for matters connected therewith or incidental thereto
6. The Goods and Services Tax (Compensation to States) Act 15 of 2017
An Act to provide for compensation to the States for the loss of revenue arising on account of
implementation of the goods and services tax in pursuance of the provisions of the Constitution (One
Hundred and First Amendment) Act, 2016.


# Functionaries




## GST

1. IGST
IGST Act doesn't define any authorities.
Under Section 4, officers appointed under SGST and UTGST also are empowered to be the officers for the purposes of the Act.
2. CGST
Most are those appointed from Central Excise Act 1944
the officers appointed under the SGST Act or the UTGST Act are authorised to be the proper officers
Any order issued under CGST Act will also be issued under SGST Act as well under intimation to the jurisdictional officer of State tax or Union territory tax
No proceedings on same subject matter if one is pending under SGST Act or UTGST Act
No rectification, appeal and revision before any officer under SGST Act or UTGST Act.

### CGST

* (4) “adjudicating authority” means any authority, appointed or authorised to pass any order or decision under this Act, but does not include the 3[Central Board of Indirect Taxes and Customs], the Revisional Authority, the Authority for Advance Ruling, the Appellate Authority for Advance Ruling, 4[the Appellate Authority, the Appellate Tribunal and the Authority referred to in sub-section (2) of section 171];
* (8) “Appellate Authority” means an authority appointed or authorised to hear appeals as referred to in section 107;
* (9) “Appellate Tribunal” means the Goods and Services Tax Appellate Tribunal constituted under section 109;
* (16) “Board” means the 1[Central Board of Indirect Taxes and Customs] constituted under the
Central Boards of Revenue Act, 1963
* (24) “Commissioner” means the Commissioner of central tax and includes the Principal Commissioner of central tax appointed under section 3 and the Commissioner of integrated tax appointed under the Integrated Goods and Services Tax Act;
* (25) “Commissioner in the Board” means the Commissioner referred to in section 168;
* (36) “Council” means the Goods and Services Tax Council established under article 279A of the Constitution;
* (99) “Revisional Authority” means an authority appointed or authorised for revision of decision or orders as referred to in section 108;

3. Officers under this Act.––The Government shall, by notification, appoint the following classes of officers for the purposes of this Act, namely:––
(a) Principal Chief Commissioners of Central Tax or Principal Directors General of Central Tax,
(b) Chief Commissioners of Central Tax or Directors General of Central Tax,
(c) Principal Commissioners of Central Tax or Principal Additional Directors General of Central T ax,
(d) Commissioners of Central Tax or Additional Directors General of Central Tax,
(e) Additional Commissioners of Central Tax or Additional Directors of Central Tax, (f) Joint Commissioners of Central Tax or Joint Directors of Central Tax,
(g) Deputy Commissioners of Central Tax or Deputy Directors of Central Tax,
(h) Assistant Commissioners of Central Tax or Assistant Directors of Central Tax, and (i) any other class of officers as it may deem fit:
Provided that the officers appointed under the Central Excise Act, 1944 (1 of 1944) shall be deemed to be the officers appointed under the provisions of this Act.


Advance Ruling Authority is as per SGST or UTGST
and so is Appellate Authority for Advance Ruling
Code of Civil Procedure, 1908
(a) discovery and inspection;
(b) enforcing the attendance of any person and examining him on oath;
(c) issuing commissions and compelling production of books of account and other records


#### Chain of Appeal

adjudicating authority
->
appellate authority(within 3/6 months, +1 month leeway, 10% dispute amount to be paid, decision within 1 year)
->
revisional authority(optional, on issue not touched - within 3 years of order or  within 1 year of appeal decided order) only based on CAG, legality and revenue, only once and only after all appeal period ended but within 3 years
->
Appellate Tribunal(GST Tribunal constituted under the CGST Act, 2017) - (within 3 months of appeal order, 20% of dispute amount, the other party should respond in 45 days - +3 months/45 days leeway, can amend its own order in 3 months upon - discharge within 1 year) - no CPC procedure but PONJ and CPC powers to summon etc,President, State President, Members, officers or other employees of the Appellate Tribunal
State Bench and Area Benches - to hear appeals against the orders passed by the Appellate Authority or the Revisional Authority - Judicial Member, one Technical Member (Centre) and one Technical Member (State)
National Bench and Regional Benches - in the cases where one of the issues involved relates to the place of supply.

National Bench - President and shall consist of one Technical Member (Centre) and one Technical Member (State).
State Bench - State President and shall consist of one Technical Member (Centre) and one Technical Member (State).
Other Benches - Judicial member, one Technical Member (Centre) and one Technical Member (State).

CG has authority over National and Regional Benches members(CJI consult) and Technical Members, and Technical Member(Centre)s of State and Area Benches
SG has authority over State and Area Benches members(CJHC consult) and Technical Member(State)s
->
High Court - only if the case involves a substantial question of law - within 180 days or receiving the order from appeal
-> Supreme Court - from National Bench or Regional Benches of the Appellate Tribunal, or from HC where HC certifies it as fit to appeal in SC



### SGST

96. (1) Andhra Pradesh Authority for Advance Ruling - (i) one member from amongst the officers of
Central tax; and
(ii) one member from amongst the officers of State
tax, to be appointed by the Central Government
and the State Government respectively.
99. Andhra Pradesh Appellate Authority for Advance Ruling for Goods and Services Tax - (i) the Chief Commissioner of Central tax as designated
by the Board; and
(ii) the Chief Commissioner of State tax:


3. The Government shall, by notification, appoint the following
classes of officers for the purposes of this Act, namely:-
(a) Chief Commissioner of State Tax,
(b) Commissioners of State Tax,
(c) Additional Commissioners of State Tax,
(d) Joint Commissioners of State Tax,
(e) Deputy Commissioners of State Tax,
(f) Assistant Commissioners of State Tax,
(g) Deputy Assistant Commissioners of State Tax,
(h) Goods and Services Tax officers, and
(i) any other class of officers as it may deem fit:

the officers appointed under the Andhra Pradesh Value Added Tax Act, 2005 shall be deemed to be the officers appointed under the provisions of this Act.

## Income Tax Act


Income Tax: Not in Syllabus but covered under upto administrative functionaries.



Central Board of Direct Taxes constituted under the Central Boards of Revenue Act, 1963

(7A) “Assessing Officer” means the 10[11[Assistant Commissioner or Deputy Commissioner] or
12[Assistant Director or Deputy Director]] or the Income-tax Officer who is vested with the relevant jurisdiction by virtue of directions or orders issued under sub-section (1) orsub-section (2) of section 120or any other provision of this Act, and the 2[Additional Commissioner or]1[Additional Director or] 2[Joint Commissioner or Joint Director] who is directed under clause (b) of sub-section (4) of that section to exercise or perform all or any of the powers and functions conferred on, or assigned to, an Assessing Officer under this Act;]
(9A) “Assistant Commissioner” means a person appointed to be an Assistant Commissioner of Income-tax 4[or a Deputy Commissioner of Income-tax] under sub-section (1) of section 117;
(9B) “Assistant Director” means a person appointed to be an Assistant Director of Income-tax under sub-section (1) of section 117;
(12) “Board” means the 7[Central Board of Direct Taxes constituted under the Central Boards of Revenue Act, 1963 (54 of 1963)];
(15A) “Chief Commissioner” means a person appointed to be a Chief Commissioner of Income- tax or a Principal Chief Commissioner of Income-tax under sub-section (1) of section 117;
(16) “Commissioner” means a person appointed to be a Commissioner of Income-tax or a Director of Income-tax or a Principal Commissioner of Income-tax or a Principal Director of Income-tax under sub-section (1) of section 117;
(16A) “Commissioner (Appeals)” means a person appointed to be a Commissioner of Income- tax (Appeals) under sub-section (1) of section 117;
(19A) “Deputy Commissioner” means a person appointed to be a Deputy Commissioner of Income-tax 7*** under sub-section (1) of section 117;
(19B) “Deputy Commissioner (Appeals)” means a person appointed to be a Deputy Commissioner of Income-tax (Appeals) 3[or an Additional Commissioner of Income-tax (Appeals)] under sub-section (1) of section 117;
(19C) “Deputy Director” means a person appointed to be a Deputy Director of Income-tax 4*** under sub-section (1) of section 117;
(21) “Director General or Director” means a person appointed to be a Director General of Income-tax or a Principal Director General of Income-tax or, as the case may be, a Director of Income-tax or a Principal Director of Income-tax, under sub-section (1) of section 117, and includes a person appointed under that sub-section to be an Additional Director of Income-tax or a Joint Director of Income-tax or an Assistant Director or Deputy Director of Income-tax;
(25) “Income-tax Officer” means a person appointed to be an Income-tax Officer under 10*** section 117;
(28) “Inspector of Income-tax” means a person appointed to be an Inspector of Income-tax under
5[sub-section (1)] of section 117;
(28C) “Joint Commissioner” means a person appointed to be a Joint Commissioner of Income-tax or an Additional Commissioner of Income-tax under sub-section (1) of section 117;
(28D) “Joint Director” means a person appointed to be a Joint Director of Income-tax or an Additional Director of Income-tax under sub-section (1) of section 117;
(29D) “National Tax Tribunal” means the National Tax Tribunal established under section 3 of the National Tax Tribunal Act, 2005;
(34A) “Principal Chief Commissioner of Income-tax” means a person appointed to be a Principal Chief Commissioner of Income-tax under sub-section (1) of section 117;
(34B) “Principal Commissioner of Income-tax” means a person appointed to be a Principal Commissioner of Income-tax under sub-section (1) of section 117;
(34C) “Principal Director of Income-tax” means a person appointed to be a Principal Director of Income-tax under sub-section (1) of section 117;
(34D) “Principal Director General of Income-tax” means a person appointed to be a Principal Director General of Income-tax under sub-section (1) of section 117;
(44) “Tax Recovery Officer” means any Income-tax Officer who may be authorised by the 9[Principal Chief Commissioner or Chief Commissioner] or 10[Principal Commissioner or Commissioner], by general or special order in writing, to exercise the powers of a Tax Recovery
Officer 11[and also to exercise or perform such powers and functions which are conferred on, or assigned to, an Assessing Officer under this Act and which may be prescribed];]




# Topics

## Constitution

265. Taxes not to be imposed save by authority of law.—No tax shall be levied or collected except by authority of law.
269. Taxes levied and collected by the Union but assigned to the States. 270. Taxes levied and distributed between the Union and the States.
271. Surcharge on certain duties and taxes for purposes of the Union.
274. Prior recommendation of President required to Bills affecting taxation in which States are interested.
276. Taxes on professions, trades, callings and employments.




-----
