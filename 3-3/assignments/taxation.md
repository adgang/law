# Padala Rama Reddi Law College Assignment

## Subject: Law of Taxation

## Income Tax Act 1961
1. Who is an assessee under the Income Tax Act, 1961?
2. Difference between previous year and assessment year.
3. Who is a deemed assessee?
4. What are the provisions u/s 10(13A) w.r.t. H.R.A?
5. What are the permissible deductions u/s 16 of the Act?
6. What are the provisions relating to self occupied property under the head income from house property?
7. What are the deductions available u/s 24 of the Act?
8. Differences between the Business and Profession under the Act?
9. What are the capital assets under the Act?
10. What is Capital Gain Account Scheme 1988?
11. What are the provisions of the Income Tax Act w.r.t section 80TTA?
12. What is T.D.S and Advance Tax?
## Goods and Services Tax 2017
13. What are indirect taxes?
14. What are the taxes subsumed under GST Act?
15. What is cascading effect?
16. Explain briefly about composite and mixed supplies.
17. What is Input Tax Credit?
18. What is Electronic Tax Liability Register under GST?
19. What is reverse charge mechanism under GST?
20. What is Summary Assessment under GST?



1. Who is an assessee under the Income Tax Act, 1961?

As per Section 2(7) of Income Tax Act 1961, assessee means a person by whom any tax or any other sum of money is payable under the Act. It includes every person on whom any proceeding under the Act has been taken for the assessment of his or any other person's assessable income/loss sustained/due refund. It also includes every person who is deemed to be an assessee or assessee in default under any provision of this Act

2. Difference between previous year and assessment year





----
