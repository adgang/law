
# In the moot court of PRR Law College

Saritha  
W/o Ritesh,  
Financial Analyst,  
XYZ Funds Limited.          

... Plaintiffs.

Vs

Ritesh,  
S/o Jagan,  
Financial Analyst,  
XYZ Funds Limited.

Aparna,
W/o Jagan


...Defendants.


Complaint Under Section 200 Read With Section 156 (3) Of the Code of Criminal Procedure For Registration of FIR Under Sections 351, 153(B), 292, 293, 295(A), 298, 109, 500 and 120 B of the INDIAN PENAL CODE.


351, 352 - assault - 3 months, 500  
503, 506 - criminal intimidation - 7 years  
440 - Mischief committed after preparation made for causing death or hurt - 5 years  
498A - husband and relative cruelty - 3 years  
320, 322, 325 - voluntarily causing grievous hurt - 7 years  
319, 321, 323 - voluntarily causing hurt - 1 year, 1000  

361 - Kidnapping from lawful guardianship  
363 - Punishment for kidnapping - 7 years  

368 - Wrongfully concealing or keeping in confinement, kidnapped or abducted person  



----
