Tue 27 Oct 2020 22:13:40 PM
----

# Acts
1. Transfer of Property Act, 1882(Act No. 4 of 1882)
2. Indian Succession Act, 1925(Act No. 39 of 1925)
2. Indian Stamp Act, 1899
3. Registration Act, 1908
4. Indian Easements Act, 1882(ACT NO. 5 OF 1882)



## Transfer of Property Act

Act No. 4 of 1882

An Act to amend the law relating to the Transfer of Property by act of Parties.

## Indian Succession Act, 1925
Act No. 39 of 1925
An Act to consolidate the law applicable to intestate and testamentary succession

## Indian Easements Act, 1882

An Act to define and amend the law relating to Easements and Licenses


## Syllabus

1. Property
2. Kinds
3. Transfer
4. Transferable and non-transferable
5. Who can transfer
5. Operation of transfer
5. Mode of transfer
5. Conditional transfer
5. Void and unlawful conditions
5. Condition precedent and condition subsequent
5. Vested and contingent interest
5. Transfer to unborn persons
5. Vested and contingent interest

6. Doctrine of Election
7. Convenants
8. Transfer by Ostensible Owner
9. Doctrine of Feeding the Grant by Estoppels
10. Doctrine of Lis Pen dens
11. Fraudulent Transfer
12. Doctrine of Part-performance

13. Sale and features
14. Mode of Sale
15. Rights and liabilities of parties
16. Mortgage & Kinds
17. Rights and liabilities of mortgagor and mortgagee
18. Marshalling and Contribution
19. Charges

20. Lease and Kinds
21. Rights and liabilities of lesser and lessee
22. Exchange
23. Gifts & types
24. Registration of Gifts
25. Transfer of Actionable Claims

26. Easements
27. Distinction between Lease and License
28. Dominant and Servant Tenements
29. Acquisition of property through testamentary succession
30. Will
31. Codicil
32. capacity to execute will
33. Nature of bequests
34. Executors of Will
35. Rights and Obligations of Legatees



Contract, elements or essential features, kinds, modes,
Rights and liabilities of parties, Termination, Forfeiture





# Unit-I

## Transfer of Property Act

Interpretation clause is Section 3 instead of 2

applies to immovable property mostly(exceptions like gift)

"instrument" means a non-testamentary instrument

"attested" - 2 or more witnesses should see the executant or his representative by the direction of executant sign the instrument on  o
has received from the executant a personal acknowledgement of his signature or mark, or of the signature of such other person
two witnesses need not be present at the same time. no form





sections 54, paragraphs 2 and 3, 59, 107 and 123 shall not extend or be extended to any district or tract of country for the
time being excluded from the operation of the Indian Registration Act

Meaning that registration as requirement is relaxed for sale and mortgage for tangible immovable property, lease for year or more(or for yearly rent), gift.

Movable property gift may be by registration or not.


Registered instrument must be attested by two witnesses.


## Property

Movable and immovable


"Immovable property" shall include land, benefits to arise out of land, and things attached to the earth, or permanently fastened to anything attached to the earth. as per General Clauses Act 1897

" immovable property" does not include standing timber, growing crops or grass;


### Transferable and non-transferable property

Non-transferable:
1. chance of heir succession
2. A right of reentry for breach of any condition. as in lessor's right to reenter on breach by lessee
3. easement -  right of way/light
4. An interest in property restricted in its enjoyment to the owner personally cannot be transferred by him
4. right of religious office
5. right to future maintenance
6. mere right to sue
7. public office or salary
8. stipend allowed to military and civil pensioners
9. right of occupancy

## Transfer

an act by which a living person conveys property, in present or in future, to one or more other living persons, or to himself and one or more other living persons; and "to transfer property" is to perform such act.

a living person includes a company or association or body of individuals

Modes:
1. By act of parties - Testamentary or In Vivos
2. By operation of law



### Capacity to transfer

1. competency to contract(not idiot or minor)
2. having ownership

A minor can only be transferee and has no burden of obligation in onerous transfers.



 Indian Contract Act, 1872
 Indian Registration Act, 1908

### Operation of Transfer - Section 8

Unless different intention is expressed or implied, all the interest of transferer is transferred to transferee:
* land - easements, rent, profits, all things attached to earth
* machinery attached to land - all movable parts
* house - easements, rent, locks, keys, bars, windows, doors meant for permanent use
* debt or actionable claim - security unless security is also for other debts not transferred, but not arrears of interest accrued
* money or other property yielding income - interest or income accrued



### Mode of Transfer - Section 9

* Oral - without writing wherever it is not expressly required by law



### Conditional Transfer

Section 10 Condition restraining alienation - Void except in the case of a lease where the condition is for the benefit of the lessor or those
claiming under him: provided that property may be transferred to or for the benefit of a
woman (not being a Hindu, Muhammadan or Buddhist), so that she shall not have power
during her marriage to transfer or charge the same or her beneficial interest therein.

Section 11 Restriction repugnant to interest created - void
when an interest therein is created absolutely in favour of any person

Section 12 Condition making interest determinable on insolvency or attempted alienation - void
Nothing in this section applies to a condition in a lease for the benefit of the lessor or those claiming
under him.

Section 13 Transfer for benefit of unborn person

Section 14: Rule against perpetuities-  "dead hand" or "mortmain".

Section 15: Transfer to class some of whom come under sections 13 and 14

Section 16: Transfer to take effect on failure of prior interest.

Section 17: Direction for accumulation

Section 18: Transfer in perpetuity for benefit of public.—



Medicines




## Sale
Sections 54-57

## Mortgage
Section 58-104

54 is mode - by registration for all > Rs 100

1. Simple
2. Conditional Sale
3. Usufructuary
4. English
5. Deposit of title deed or Equitable mortgage
6. Anomalous


Sub-mortgage

Puisne mortgage

1. Right to Sue
2. Power of Sale
3. Power of Foreclosure
4. Power of Possession
5. Power to Appoint a Receiver

1. Right of redemption or Doctrine of equitable redemption or once a mortgage(Section 60)
2. Power to lease


Implied contract
1. he has interest and power to transfer
2. he will defend title
3. pay all public charges
4. if lease, pay all rents and that he had paid all rents and met all conditions
5. 2nd mortgage, to pay interest on earlier mortgages and to pay all such principals

1. Duty not to waste and reduce value of property
2. duty to defend title
3. pay all public charges
4. pay all rents if lease



Mortgage vs Charge

Charge denotes an impediment over the title of the property


## Charge
Sections 100-102





## Lease
Section 105-117

Lease vs License?

## Exchange
Section 118-121




## Gift
Sections 122-129

“Gift” is the transfer of certain existing moveable or immoveable property made voluntarily and without consideration, by one person, called the donor, to another, called the donee, and accepted by or on behalf of the donee.

Such acceptance must be made during the lifetime of the donor and while he is till capable of giving, If the donee dies before acceptance, the gift is void.

Transfer: registered instrument signed by or on behalf of the donor, and attested by at least two witnesses

Gift of future property is void.

Gift to several of whom one doesn't accept is void to the extent of interest he would have taken if he accepted the gift.

Gift may be suspended or revoked on the happening of a contingent event agreed between donor and donee, if the event's happening does not depend on will of donor. Any gift is void to the extent the contingent event is dependent on will of donor.

A gift also be revoked in any of the cases (save want or failure of consideration) in which, if it were a contract, it might be rescinded.

Otherwise a gift cannot be revoked.

Nothing contained in this section shall be deemed to affect the rights of transferees for
consideration without notice. meaning that any such revocation or recision have to take place with notice.

127. Onerous Gifts: Gifts which come burdened with an obligation.

In case of a gift in the form of a single transfer of several things of which even one is onerous, the donee can take nothing by the gift unless he accepts it fully.

In case of a gift in the form of two or more separate and independent transfers of which one is beneficial and the other is onerous, the donee is at liberty to accept one or few of them and refuse the others.

Onerous gift to disqualified person.—A done not competent to contract and accepting
property burdened by any obligation is not bound by his acceptance. But if, after becoming
competent to contract and being aware of the obligation, he retains the property given, he
becomes so bound.


128. Universal donee: When the gift is donor's entire property, the done is personally liable for all the debts due by and liabilities of the donor at the time of the gift to the extent of the property comprised therein.

Note: only liable for all debts due by and liabilities of donor at the time of gift.

129. Saving of donations mortis causa and Muhammadan law






## Transfer of Actionable Claims
Section 130-137




## Easements

Section 4 - An easement is a right which the owner or occupier of certain land possesses, as such, for the beneficial enjoyment of that land, to do and continue to do something, or to prevent and continue to prevent something being done, in or upon, or in respect of, certain other land not his own.


The land for the beneficial enjoyment of which the right exists is called the dominant heritage, and the owner or occupier thereof the dominant owner; the land on which the liability is imposed is called the servient heritage, and the owner or occupier thereof the servient owner.

Easements are either
1. continuous or
2. discontinuous

1. apparent or
2. non-apparent.

Easements are restrictions of one or other of the following rights (namely):—
(a) Exclusive right to enjoy.—The exclusive right of every owner of immovable property (subject to any law for the time being in force) to enjoy and dispose of the same and all products thereof and accessions thereto.
(b) Rights to advantages arising from situation.—The right of every owner of immovable property (subject to any law for the time being in force) to enjoy without disturbance try another the natural advantages arising from its situation.


## Will


PART VI of Indian Succession Act 1925

2(h) “will” means the legal declaration of the intention of a testator with respect to his property
which he desires to be carried into effect after his death.

2(c) “executor” means a person to whom the execution of the last Will of a deceased person is, by
the testator's appointment, confided;

2(f) “probate” means the copy of a will certified under the seal of a court of competent jurisdiction with a grant of administration to the estate of the testator;


Section 59



Unprivileged will - two witnesses
Privileged will

Will void on marriage


Legacy a gift of personal (as opposed to real) property by will
A general legacy, a demonstrative legacy, and a specific legacy represent the three primary types of legacies.

 4.-2. A specific legacy is a bequest of a particular thing, or money specified and distinguished from all other things of the same kind; as of a particular horse, a particular piece of plate

bequest - n. the gift of personal property under the terms of a will.

## Codicil

2(b) “codicil” means an instrument made in relation to a Will, and explaining, altering or adding to its dispositions, and shall be deemed to form part of the Will;


Ademption



Bequests:

* residuary beques
* Bequest in alternative.
* Effect of words describing a class added to bequest to person
* Bequest to religious or charitable uses
* Onerous bequests
* Bequest contingent upon specified uncertain event, no time being mentioned for its occurrence
* Bequest over, conditional upon happening or not happening of specified uncertain event.
* Bequests with Directions as to Application or Enjoyment
* Bequests to an Executor

Probate Vs Letter of Administration




5 min rush









----
