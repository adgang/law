Mon 11 Mar 2019 13:17:00 PM
-------

# Land Law

### The  AP Land Reforms(Ceiling on Agriculture Holdings) Act, 1973

Status in determination of surplus lands

What to do with surplus lands?


first step is declaration.(as excess land)



Anyone with holding more than 10 acres wetland or 25 acres of dry land should file a declaration.

Definition of holding: Land held by a person either in capacity of owner, limited owner, tenant, lessee, mortgagee etc.

Who should get declaration?


Tribunal(RDO)/Appellate tribunal


Declaration should have lands which are owned outside state.

Notification date: 1-1-1975
Within 30 days from notification date.

Only one declaration is enough before one RDO.

RDO can issue notice requiring a person to furnish declaration. If still declaration is not filed, RDO may find out on his/her own.

Enquiry after publication and pass an order either the person is excess land holder or not and how much.

This is called Determination of ceiling area.


Date of acquisition becomes notified date.

1. Declaration
2. Determination
3. Surrender

RDO should give notice for surrender.

which of the lands does the declarant wants to surrender?

Declarant should furnish statement listing the lands to surrender.

RDO should approve surrender lands.

RDO has option to select any of the land for purpose of surrender if declarant did not file statement.

What about encumbrance or disputed lands? RDO can reject such lands.

Lands that cannot be used for agriculture in the future also can be refused.

Who can file? Karta of joint family, guardian of minor,


Long Answers:
What are salient features of land ceiling Act?

Short Answers:
Standard Holding



Doubt: disputed lands counted in surrender


Female members should not lose more than average.

Protected tenant: Protected tenant stands on a different footing. Will be discussed in Unit 2.


After RDO approves surrender, he should pass an order of taking possession. Then land is deemed to be vested in state free from all encumbrances.


Reversion: When tenant has surplus land, it reverts to owner.

This land is alienated not in contradiction of POT Act or

This is not assigned land.  

Assignment is done on condition of non transfer and on basis of payment. 15 years installments

1961 Land Ceiling - unit of application is individual.

Max 24 acres for individual.

Only 55000 acres of surplus were acquired.






----
