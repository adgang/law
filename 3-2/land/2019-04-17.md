Wed 17 Apr 2019 11:45:00 AM
-------

# Land Law

## Right to Fair Compensation and Transparency in Land Acquisition, Rehabilitation and Resettlement Act, 2013(Land Acquisition Act)

Two topics today


1. Determination of public purpose and social impact
2. Special Provision for Food Safety

### Determination of public purpose and social impact

1. Preliminary Investigation
2. Appraisal by Expert Committee
3. Decision of government

Investigation is called social impact assessment. This investigation should be done within 6 months from date of notification. This should be done in consultation with local authorities.

Determine:
1. purpose
2. whether the extent is justified
3. alternative remedies considered

and report within 6 months.

The report shall be evaluated by an independent multi-disciplinary committee called expert committee.


Government should present its differing opinions. Two months for this evaluation.


not in contra of Scheduled Areas Land Transfer(LTR) Act.

Private - 80% of public consent
Public - 70% of public consent


All three must be published in
1. affected area and circulated
2. among local officers/authorities.
3. These should also be uploaded on website of government.


That area shouldn't have any un-utilized previously acquired land.

Irrigated multi-crop land can never be acquired.
Equivalent waste land should be set aside
Or land value should be deposited and used for promotion of agriculture.


More than 50% land cannot be acquired.

Railways, National Highways, District Roads are exempt.

Social Impact Assessment can be exempted during urgencies.

### Special Provision for Food Safety


Questions:
1. Salient features of Act
2. Procedure for acquiring land under the Act


-----
