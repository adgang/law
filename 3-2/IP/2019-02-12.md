Tue 12 Feb 2019 10:20:24 AM
-------

## Intellectual Property

1. Copyright
2. Patent
3. Trademark
4. Industrial Design
5. Geographical Indicators
6. UPOV -  International Union for the Protection of New Varieties of Plants

### Lesson Plan
* Introduction - when, which act enacted
* Authorities - where to apply, objections
* Jurisdiction - civil or criminal, sometimes administrative with tribunals
* Procedures
* Liability and Punishments


Droit Right: Inalienable and Unassignable right

```
Property
|
|----Tangible
|       |
|       |--- Movable
|       |--- Immovable
|
|----Intangible
        |
        |---- Personal -- Copyright
        |
        |---- Industrial
                  |
                  |-- Patent
                  |-- Trademark
                  |-- Industrial Design
                  |-- Geographical Indications
                  |-- UPOV
```
