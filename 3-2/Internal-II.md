1st day

1. Labour -II
2. Public International Law
3. Interpretation of Statutes

# Labour-II

Units III, IV, V

Whitley Commission  
Full Bench formula  
Bonus Commission  

Payment of Wages Act 1936  
Minimum Wages Act 1948  
The Employee’s Compensation Act 1923  

Employees State Insurance Act 1948  
Payment of Bonus Act 1965  
Employees Provident Fund and Miscellaneous Provisions Act 1952  
The Maternity Benefit Act 1961  
The Payment of Gratuity Act 1972  
The Factories Act 1948  
Child Labour (Prohibition and Regulation) Act 1986  
The Equal Remuneration Act, 1976  


# Public International Law

Units III, IV, V

United Nations Convention on the Law of the Seas  
Conventions relating to Airspace  
Paris, Havana, Warsaw and Chicago Conventions  

Conventions such as

Outer space Treaty  
Agreement on Rescue and Return of Astronauts  
Liability Convention  
Agreement on Registration of Space objects  
Moon Treaty  
Unispace  

League of Nations  
United Nations  
International Court of Justice  
International Criminal Court  
Specialized agencies of the UN — WHO, UNESCO, ILO, IMF and WTO  



# Interpretation of Statutes

Units III, IV, V

Rules of Construction under the General Clauses Act 1897

# Land


# Intellectual Property



-----
