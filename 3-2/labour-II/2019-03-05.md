Tue 05 Mar 2019 10:07:00 AM
-------

# Labour-II

## Section 7 - Deductions

2. Permissible deductions:

ii. contribution to National Defense Fund or Defense Savings Scheme  
j. Payments to co-operative societies or Indian Post Office insurance schemes(Sec-13)  
k. Payment of premium of LIC or for purchase of government securities or deposits into Post Office Savings Bank(Sec-13)  
kk. Contribution to any fund by any RTU(Registered Trade Union)  
kkk. Membership fee of any RTU  
l. insurance premium on fidelity guarantee bonds  
m. Recovery of losses sustained by railway administration on account of counterfeit coins, mutilated, or forged currency notes(Sec-10)  
n. recovery of losses sustained by railway administration due to failure of the employee to invoice bill, collect the fare or sale of food etc.(Sec-10)  
o. recovery of losses sustained by railway administration on account of negligently or incorrectly levied rebates or refund(Sec-10)  
p. towards any PM relief fund or central government fund  
q. contribution to any insurance scheme framed by central government for the benefit of the employee

### Section 7(3) - Maximum Deductions

The total amount of deductions from the wages of a wage period shall not exceed
1. 75% of the wages if he contributes to any co-operative societies
2. 50% of the wages in any other case

### Section 7(4) - Deductions as per other laws

The employer can recover from wages any amount payable by the employee under any law for the time being in force.

### Section 8 - Fines

1. The employer shall not levy fines unless the finable acts or omissions are listed, and approved and exhibited in advance.
2. All the fine amounts collected shall be recorded in register and used only for the benefit of the employees.
3. Opportunity to show cause against fines should be given.
4. Total amount of fine in a wage period shall not exceed 3% of the wages of that wage period.
5. No fine for a child upto 15 years of age.
6. Fines cannot be recovered in installments or after the expiry of 90 days from the day on which it was imposed.
----
