Mon 18 Feb 2019 09:38:00 AM
-------

# Labour-II

## Minimum Wages Act 1948(MINWA 1948)

Another a person who employs one or more employees in any scheduled employment(SE) is an employer.

Any person employing persons in any scheduled employment is not an employer, within the meaning of the act unless minimum wages have been fixed under the Act in respect of that employment.

#### Mathuram S Vs State of MP 1966

A person who engages workers through a contractor would also be an employer.

Neither the principal employer nor the contractor can be liable to pay minimum wages if it is not notified under the Minimum Wages Act.

#### Robert Toppo Vs State of Jharkhand and Ors
The petitioner who is the principal of a school in which a hall is being constructed and the laborers were working on the construction.

On the report of a labor enforcement officer, an order was passed asking him to pay minimum wages to the workers. He contended that they are the guardians of a few students helping the school by Shramadaan.

The HC held that the petitioner was an employer under Section 2E of the Act.

Scheduled employments are those listen in Schedules of MINWA 1948 and includes employments that form part of the processes/employments in the Schedules.

Scheduled employment means an employment specified, any branch of work forming part of such employment.

The following are such employments in the Schedule.

### Part-I
12 entries include:  
1. Woolen Carpet Making
2. Shawl Weaving
3. Rice/Dal/Flour Mill
4. Tobacco manufacturing
5. Plantations
6. Oil Mill
7. Employment under any local authority
8. Building Operations
9. Construction of Roads
10. Stone breaking/Crushing
11. Lac manufacturing
12. Mica Works
13. Public Motor Transport
14. Tanneries
15. Leather Manufacturing

### Part-II
1. Agricultural Operations
2. Forestry
3. Timbering Operations
4. Dairy Farming
5. Poultry
6. Livestock
7. Sericulture
8. Post-agricultural operations etc.

## Wages

### Payment of Wages Act 1936
Wages means all remuneration expressed in terms of money which would, if the terms of the contract of employment, express or implied, were fulfilled, be payable to a person employed in respect of his employment or of work done in such employment
2) and includes HRA
3) but does not include  
    value of any house accommodation, supply of water, light and medical attendance or any amenity or service excluded by the government.  
    employers contribution to pension fund, provident fund or any insurance scheme.  
    any traveling allowance, gratuity, any special expenses.



----
