Mon 06 Mar 2019 10:26:00 AM
-------

# Public International Law

# Citizenship

To resolve these problems of double nationality of married women, a convention of nationality of married women was adopted by the General Assembly of the United Nations on 29th January 1957.

Article 1 of the convention neither the dissolution of a marriage between a state's national and an alien

nor the challenge of her nationality by her husband during marriage shall automatically effect the nationality of the wife.


The Hague Conference 1930 Articles 8-11 provides that if a woman marries another national she automatically acquires the nationality of her husband. However she can retain her birth nationality.

On December 18 1979, the General Assembly of UN adopted the Convention on Elimination of all forms of Discrimination Against Women.

According to Article 9 of the convention, state parties agreed to grant women equal rights with men to acquire, change or retain their nationality. State parties shall also grant women equal rights with respect to nationality of their children.

Treaties form an important source of international law. If an agreement subject to certain rights, duties and international obligations is entered between the heads of the state, or diplomatic envoys or international organizations, it is called as treaty.

Treaty is an agreement between two or more states, whereby they undertake to carry out obligations imposed on each of them.

According to J G Starke, in nearly all the cases, the object of the treaty is to impose binding obligations on the states who are parties to it.

### Pacta Sunt Servanda

The basis of the source for the binding force of treaties in international law is the doctrine of `pacta sunt servanda`. It is borrowed from Roman law and has been adopted as a principle governing treaties in international law.

According to this doctrine, the parties to the treaty are bound to observe its terms in good faith.

Dionisio Anzilotti formulated this theory and regarded this as the source of international law.

States which fulfill the conditions of statehood in international law or international organisations can be parties to treaties.

The life of international community is based not only on relations between states but also to an increasing degree of relation between state and foreign individuals or corporations.

No economic relation can exist between nations without the principle of Pacta Sunt Servanda.


Vienna Convention on Diplomatic Relations 1961


#### Vienna Convention on the Law of Treaties 1969

The principle of `pacta sunt servanda` has been incorporated in the Vienna Convention on Law of Treaties 1969.

Article 26 of the convention provides that every treaty in force is binding upon the parties to it. And must be performed by them in good faith. Vienna Convention on the Law of Treaties 1969.

In view of the significance of the Law of Treaties, the international law commission decided in 1975 to attempt its codification in the draft convention on the law of treaties.


The commission completed its work in 1966 and on 23rd May 1969, the United Nations conference on the law of treaties adopted the Vienna Convention on the Law of Treaties.


This Convention is a major of codification and progressive development of the Law of Treaties.

Classification of Treaties:
1. Unilateral treaties
2. Bilateral - which bind both parties to the treaty
3. Multilateral - which bind more than two states

According to L Oppenheim, treaties are classified into two.

1. Law making Treaties
2. Treaties for other purposes

Law making treaties are concluded for the purpose of laying down general rules of conduct among considerable number of states.

Ex: United Nations Charter

Other category: Treaties which are concluded for any other purpose.


### Formation of Treaties

One question on this

Formation or conclusion of treaties:

1. Accrediting of persons or representatives
2. Negotiation
3. Authentication and Signature
4. Ratification
5. Accession
6. Entry into Force
7. Registration and publication
8. Application and Enforcement







----
