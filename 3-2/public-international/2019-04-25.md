
6. Reasonable prima-facie evidence
To seek extradition, there must be reasonable prima-facie evidence of guilt of the accused for the crime for which the extradition is requested.

7. Treaty formalities:
To invoke extradition, the terms and conditions of the extradition must be strictly complied with.

Savarkar case:

1911 - Savarkar was an Indian revolutionary leader. He was charged with the offense of high treason and abetment of murder.

He was being brought to India from the United Kingdom in a British trail ship for prosecution. But he had escaped at the port of Marseilles and was apprehended by French police. The French police brought him back to the ship and handed over to the captain of the ship without any extradition proceedings. The French government demanded the return of the prisoner and required a formal demand to be made for his extradition.

French government contended that the delivery of Savarkar to the British government is contrary to international law. But the British government declined to surrender Savarkar. The permanent court of arbitration gave its decision in 1911 and the case was with much controversy and it considered and involved an international question of right of asylum.

The court held firstly that since there was a pattern of collaboration between the countries regarding the possibility of Savarkar's escape in Marseilles and there was neither force nor fraud in inducing the French authorities to return Savarkar to them, the British authorities did not have to hand Savarkar back to the French in order for the latter to hold proceedings.

International law does not impose any obligation upon the state whereby the criminal may be returned.

Once the person is extradited even by mistake or in an irregular way, the receiving state is not bound under international law to return him.

The decision was subject to criticism and is not based on the principles of international law.


## Asylum

The term asylum is derived from a and sylum which means 'not'
 and right of seizure respectively. Asylum means a sanctuary or a place of refuge. Asylum means protecting and providing shelter on request and subject to control by a state to a political refugee of another state.

 In other words, if a person commits a crime and fled for another state, to take shelter in another country is called asylum.


 According to Starke, the concept of asylum involves two elements.
 1. shelter
 2. a degree of active protection on the part of the authorities in control of the territory of asylum

 Expedition differs from asylum

Expedition | Asylum
------------|----
 Expedition means delivery of an accused or a convicted individual to the state on whose territory he is alleged to have committed or having convicted of crime. | In asylum, the active protection extended to a political refugee from another state by a state which admits him on his request.



1. UDHR(Universal) on 10th December 1948 also asserts a right of asylum. Everyone has the right to seek and to enjoy in other countries asylum from persecution.
2. The right may not be invoked in case of non-political crimes or from the acts of contrary to the purpose and principles of the UN.

### Kinds of asylum
There are two kinds of asylum.
1. Territorial or internal asylum
2. Extra-territorial or diplomatic asylum

If the asylum is granted by the state on its territory, it is called territorial asylum. It is an indication of territorial sovereignty of the state granting asylum.

If an individual leaves this country and asks for asylum from the state he wishes to settle in, it is territorial asylum.



12:27

According to international law, the sovereign states have the right to grant asylum within its territory on its discretion.

The General Assembly also recognised, under Article 14 of UDHR, the right to seek and enjoy in other countries, asylum from persecution.

The grant of territorial asylum to Dalai Lama and his Tibetan followers within the territory of India is an example of territorial asylum.


Extra territorial asylum - The grant of asylum by a state outside its own territory is said to be extra-territorial asylum.

In other words, the grant of asylum on places not forming the physical territory is said to be extra-territorial asylum.

Extra-territorial asylum may be given at any of the following places.
1. Asylum in legation or diplomatic asylum
The grant of asylum by a state in its embassy premises situated in a foreign state is said to be asylum in legation or diplomatic asylum. It is so because the embassy premises are considered to be excluded from the territorial jurisdiction of the state where it is situated. Asylum in consular premises.

The grant of asylum relating to legation premises are applicable to the grant of asylum in consular premises.

3. Asylum in the premises of international institutions
Generally international law does not permit asylum in the premises of international institutions. However temporary asylum can be granted in case of danger of imminent violence.

Asylum in warship. It may be granted to political offenders. It may be granted in warships because men of war and public vessels of a foreign country are exempted from the jurisdiction of the state in whose ports or waters it may be found. Rather they are under the jurisdiction of the flag state.


Asylum in the case of merchant vessels: Merchant vessels are subjected to local jurisdiction and hence they are not competent to give asylum.


The reason that merchant vessels cannot grant asylum that they are not excluded from the jurisdiction of the state in whose port or water it is found and it can be given asylum only when there is a treaty between the states.


Differences between Territorial and Extra-Territorial asylum

One is an act of asserting sovereignty. The other is a derogation of sovereignty.


Every state has a right to grant asylum under territorial asylum unless it has accepted some particular restriction in this regard.

Right to grant extra-territorial asylum is exceptional and must be established in each case.

-----
