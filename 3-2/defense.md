
# In the moot court of PRR Law College


Ritesh,  
S/o Jagan,  
Financial Analyst,  
XYZ Funds Limited.

Aparna,
W/o Jagan


...Accused.


Vs


Saritha  
W/o Ritesh,  
Financial Analyst,  
XYZ Funds Limited.          

... Complainant.



Accused 1 is a loving father and Accused 2 has been a doting grandmother.

Accused 1 saw the complainant at their office unexpectedly.  He had to confront her and ask why she has been behaving in such a way recently and tried to make up. But at no point did he intend to hurt the complainant. The prosecution failed to prove any mens rea of my client.

Accused 1 had just realised that he cannot have any more children. It is understandable that he was upset and hence the alleged threats had no intention of harming anyone, let alone his own wife.

Accused 1 and Accused 2 genuinely wanted a third child in the family and categorically reject the allegation that they insisted on a male child or harassed the complainant towards that purpose. It is a part of marital tradition in India that in-laws and parents give opinions and hope their children have more kids. At no position did my clients cause any hurt to the opposing counsel's client.

Accused 1, realizing that his wife is not paying enough attention to the two kids, put them in the care of responsible caretakers. The allegation that this is done to steal the children away is baseless and my client has only the care of the children in his mind, especially considering the circumstances which would cause the children to be more stressful. It is unthinkable that Accused 1, who has a comparable job to the complainant will sell his children. He would never have that thought nor necessity.



A rush of emotions gripped him as the memories of last few days came by and he could not control his rage.



3A

322 - grievous hurt  
359

503 A - threat to kill  

498A - cruelty  


120 A - conspiracy  
370 - human trafficking  


evidence

no one sells

what threatened

family planning

no notice

502

abduction from lawful guardian




-----
