http://www.osmania.ac.in/lawcollege/syllabi.html


FACULTY OF LAW, OSMANIA UNIVERSITY
# SYLLABUS OF LL.B. (3YDC) THREE-YEAR DEGREE COURSE
(WITH EFFECT FROM 2009-2010  )
## LL.B. I SEMESTER

### PAPER-I:  LAW OF CONTRACT–I

#### Unit-I :
Definition and essentials of a valid Contract - Definition and essentials of a valid Offer - Definition and
essentials  of  valid  Acceptance  -  Communication  of  Offer  and  Acceptance  -  Revocation  of  Offer  and  Acceptance
through various modes including electronic medium - Consideration - salient features - Exception to consideration -
Doctrine of Privity of Contract - Exceptions to the privity of contract - Standard form of Contract.
#### Unit-II  :
Capacity  of  the  parties  -  Effect  of  Minor's  Agreement  -  Contracts  with  insane  persons  and  persons
disqualified by law - Concepts of Free Consent - Coercion - Undue influence - Misrepresentation - Fraud - Mistake -
Lawful  Object  -  Immoral  agreements  and  various  heads  of  public  policy  -  illegal  agreements  -  Uncertain
agreements  - Wagering agreements - Contingent contracts - Void and Voidable contracts.
#### Unit-III:
Discharge of Contracts - By performance - Appropriation of payments - Performance by joint promisors -
Discharge  by  Novation  -  Remission  -  Accord  and  Satisfaction  -  Discharge  by  impossibility  of  performance
(Doctrine of Frustration) - Discharge by Breach - Anticipatory Breach - Actual breach.
#### Unit-IV  :
Quasi  Contract  -  Necessaries  supplied  to  a  person  who  is  incapable  of  entering  into  a  contract  -
Payment by an interested person - Liability to pay for non-gratuitous acts -  Rights of finder of lost goods - Things
delivered by mistake or coercion - Quantum merit - Remedies for breach of contract - Kinds of damages - liquidated
and unliquidated damages and penalty - Duty to mitigate.
#### Unit-V :
Specific Relief - Recovering possession of property - Specific performance of the contract - Rectification of
instruments  -  Rescission  of  contracts  -  Cancellation  of  instruments  -  Declaratory  Decrees  -  Preventive  Relief  -
Injunctions - Generally - Temporary and Perpetual injunctions - Mandatory & Prohibitory injunctions - Injunctions
to perform negative agreement.
#### Suggested Readings:
1.
Anson:
Law of Contract
, Clarendon Press, Oxford, 1998.
2.  Krishnan Nair:
Law of Contract
, S.Gogia & Co., Hyderabad 1995.
3.  G.C.V. Subba Rao:
Law of Contract
, S.Gogia & Co., Hyderabad 1995.
4.  T.S.Venkatesa Iyer:
Law of Contract
, revised by Dr. Krishnama Chary, S. Gogia & Co.
5.  Avtar Singh:
Law of Contract
, Eastern Book Company, Lucknow, 1998.

### PAPER-II: FAMILY LAW–I (Hindu Law)
#### Unit-I  :
Sources of Hindu  Law – Scope  and application of Hindu  Law – Schools  of Hindu  Law - Mitakshara  and
Dayabhaga  Schools  –  Concept  of  Joint  Family,  Coparcenary,  Joint  Family  Property  and  Coparcenary  Property  –
Institution  of  Karta-   Powers  and  Functions  of  Karta  -  Pious  Obligation  -  Partition  –  Debts  and  alienation  of
property.
#### Unit-II  :
Marriage  -  Definition  -  Importance  of  institution  of  marriage  under  Hindu  Law  –  Conditions  of  Hindu
Marriage – Ceremonies and Registration – Monogamy – Polygamy.
Unit-III: Matrimonial
Remedies under the Hindu Marriage Act, 1955 - Restitution of Conjugal Rights – Nullity of
marriage – Judicial separation – Divorce – Maintenance
pendente lite
– importance of conciliation.
#### Unit-IV:
Concept  of  Adoption  - Law  of  Maintenance  - Law  of  Guardianship  -  Hindu  Adoption  and  Maintenance
Act, 1956 – Hindu Minority and Guardianship Act 1956.
#### Unit-V :
Succession – Intestate succession – Succession to the property of Hindu Male and Female; Dwelling House
– Hindu Succession Act, 1956 as amended by the Hindu Succession (Andhra Pradesh Amendment) Act, 1986 & the
Hindu Succession (Amendment) Act, 2005 – Notional Partition – Classes of heirs – Enlargement of limited estate of
women into their absolute estate.
#### Suggested Readings:
1.   Paras Diwan :
Modern Hindu Law
, 13th Edition 2000, Allahabad Agency, Delhi.
2.   Paras Diwan
: Family Law
, 1994 Edition, Allahabad Agency, Delhi.
3.   Mayne:
Hindu Law - Customs and Usages
, Bharat Law House, New Delhi.
4.  Sharaf:
Law of Marriage and Divorce
, 1999.

### PAPER-III : CONSTITUTIONAL LAW-I
#### Unit-I
Constitution-Meaning and Significance - Evolution of Modern Constitutions -Classification of Constitutions-
Indian  Constitution  - Historical  Perspectives  - Government  of  India  Act,  1919  - Government  of  India  Act,  1935  -
Drafting of Indian Constitution - Role of Drafting Committee of the Constituent Assembly
#### Unit-II
Nature  and  Salient  Features  of  Indian  Constitution  -  Preamble  to  Indian  Constitution  -  Union  and  its
Territories-Citizenship - General Principles relating to Fundamental Rights(Art.13) - Definition of State
#### Unit-III
Right to Equality(Art.14-18) – Freedoms and Restrictions under Art.19 - Protection against Ex-post facto
law - Guarantee against Double Jeopardy - Privilege against Self-incrimination - Right to Life and Personal Liberty -
Right to Education – Protection against Arrest and Preventive Detention
#### Unit-IV
Rights  against  Exploitation  - Right  to  Freedom  of  Religion  - Cultural  and  Educational  Rights  - Right  to
Constitutional Remedies - Limitations on Fundamental Rights(Art.31-A,B and C)
#### Unit-V :
Directive  Principles  of  State  Policy  –  Significance  –  Nature  –  Classification  -   Application  and  Judicial
Interpretation  -  Relationship  between  Fundamental  Rights  and  Directive  Principles  -  Fundamental  Duties  –
Significance - Judicial Interpretation

#### Suggested Readings
1. M.P.Jain,
Indian Constitutional Law
, Wadhwa & Co, Nagpur
2. V.N.Shukla,
Constitution of India
, Eastern Book Company, Lucknow
3. Granville Austin
, Indian Constitution-Cornerstone of a Nation
, OUP, New Delhi
4. H.M.Seervai,
Constitutional Law of India
(in 3 Volumes), N.M.Tripathi, Bombay
5. G.C.V.Subba Rao
, Indian Constitutional Law
, S.Gogia & Co., Hyderabad
6.  B.Shiva  Rao:
Framing  of  India’s  Constitution
(in  5  Volumes),  Indian  Institute  of  Public  Administration,  New
Delhi
7. J.N.Pandey,
Constitutional Law of India
, Central Law Agency, Allahabad

### PAPER-IV:   LAW   OF   TORTS   INCLUDING   MOTOR   VEHICLE   ACCIDENTS   AND   CONSUMER PROTECTION LAWS
#### Unit-I :
Nature of Law of Torts - Definition of Tort - Elements of Tort - Development of Law of Torts in England
and India - Wrongful Act and Legal Damage -
Damnum Sine Injuria
and
Injuria Sine Damnum
- Tort distinguished
from  Crime  and  Breach  of  Contract  - General  Principles  of  Liability  in  Torts  - Fault  - Wrongful  intent  - Malice  -
Negligence - Liability without fault - Statutory liability - Parties to proceedings.
#### Unit-II
General Defenses to an action in Torts – Vicarious Liability - Liability of the State for Torts – Defense of
Sovereign Immunity – Joint Liability – Liability of Joint Toreadors – Rule of Strict Liability (
Ryland’s V Fletcher
) –
Rule  of  Absolute  Liability  (
MC  Mehta  vs.  Union  of  India
)  – Occupiers  liability  – Extinction  of  liability  – Waiver
and Acquiescence – Release – Accord and Satisfaction - Death.
#### Unit-III
Specific  Torts  -  Torts  affecting  the  person  -  Assault  -  Battery  -  False  Imprisonment  -  Malicious
Prosecution - Nervous Shock - Torts affecting Immovable Property - Trespass to land - Nuisance - Public Nuisance
and Private Nuisance - Torts relating to movable property – Liability arising out of accidents (Relevant provisions of
the Motor Vehicles Act).
#### Unit-IV
Defamation - Negligence - Torts against Business Relations - Injurious falsehood - Negligent Misstatement
- Passing  off  -   Conspiracy  - Torts  affecting  family  relations  - Remedies  -  Judicial  and  Extra-judicial  Remedies –
Damages – Kinds of Damages – Assessment of Damages – Remoteness of damage - Injunctions - Death in relation
to tort -
Action personalize moritur cum persona.
#### Unit-V
Consumer  Laws:  Common  Law  and  the  Consumer  -  Duty  to  take  care  and  liability  for  negligence  -
Product  Liability  -  Consumerism  -  Consumer  Protection  Act,  1986  -  Salient  features  of  the  Act   -   Definition  of
Consumer - Rights of Consumers - Defects in goods and deficiency in services – Unfair trade practices
-
Redressal
Machinery under the Consumer Protection Act - Liability of the Service Providers, Manufacturers and Traders under
the Act – Remedies.

#### Suggested Readings:
1.  Winfield & Jolowicz :
Law of Tort
, XII edition, Sweet and Maxwell, London , 1984.
2.  Salmond and Heuston :
Law of Torts
, XX edition, 2nd Indian reprint, Universal Book traders, New Delhi,1994.
3.  Ramaswamy Iyyer:
The Law of Torts
, VII edition (Bombay, 1995).
4.  Achutan Pillai:
Law of Tort
, VIII edition , Eastern Book Company, Lucknow, 1987.
5.  Durga Das Basu:
The Law of Torts
,X edition, Prentice Hall of India, New Delhi, 1998.
6.   Ratan Lal & Dhirajlal:
The Law of Torts
, 22nd edition, Wadhwa & Company Nagpur, 1992.
7.  R.K.Bangia:
Law of Torts
, XIV edition, Allahabad Law Agency, Allahabad, 1999.
8.  J.N.Pandey:
Law of Torts
, 1st edition Central Law Publications, Allahabad, 1999.
9.   Vivienne Harpwood:
Law of Torts
, 1st edition, Cavendish Publishing Ltd. London, 1993.
10.Hepple & Mathews:
Tort - Cases and Materials
, 2nd edition  Butterworth, London, 1980.
D.N.Saraf: Law of Consumer Protection in India, Tripati, Bombay
The Motor Vehicles Act, 1988


### PAPER–V: ENVIRONMENTAL LAW
#### Unit-I
The  meaning  and  definition  of  environment  –  Ecology  -  Ecosystems-Biosphere  -  Biomes  -  Ozone
depletion  -  Global  Warning  -  Climatic  changes  -  Need  for  the  preservation,  conservation  and  protection  of
environment  - Ancient  Indian  approach  to  environment- Environmental  degradation  and  pollution  - Kinds,  causes
and effects of pollution.
#### Unit-II
Common Law remedies against pollution - trespass, negligence, and theories of Strict Liability & Absolute
Liability  - Relevant  provisions  of  I.P.C.  and  Cr.P.C.  and  C.P.C.,  for  the  abatement  of  public  nuisance  in  pollution
cases - Remedies under Specific Relief Act - Reliefs against smoke and noise - Noise Pollution.
#### Unit-III
The  law  relating  to  the  preservation,  conservation  and  protection  of  forests,  wild  life  and  endangered
species, marine life, coastal ecosystems and lakes etc. - Prevention of cruelty towards animals - The law relating to
prevention and control of water pollution - Air Pollution - Environment pollution control mechanism - Law relating
to environment protection – National Environmental Tribunal and National Environmental Appellate Authority.
#### Unit-IV:
Art.  48A  and  Art.  51A(g)  of  the  Constitution  of  India  -  Right  to  wholesome  environment  -  Right  to
development -  Restriction on freedom of trade, profession, occupation for the protection of environment - Immunity
of Environment legislation from judicial scrutiny(Art.31C) - Legislative powers of the Centre and State Government -  Writ jurisdiction - Role of Indian Judiciary in the evolution of environmental jurisprudence.

#### Unit-V
International  Environmental  Regime  - Transactional  Pollution  - State  Liability  - Customary International
Law - Liability of Multinational Corporations/Companies - Stockholm Declaration on Human Environment, 1972 -
The  role  of  UNEP  for  the  protection  of  environment  -  Ramsar  Convention  1971  –  Bonn  Convention  (Migratory
Birds) 1992 -  Nairobi Convention, 1982 (CFCC) - Biodiversity Convention (Earth Summit), 1992 -  Kyoto Protocol
1997, Johannesburg Convention 2002.
#### Suggested Readings:
1.   Paras Diwan:
Studies on Environmental Cases
.
2.   S.N. Jain (ed.):
Pollution Control and the Law
.
3.   Armin Rosencranzand Shyam Divan:
Environmental Law and Policy in India
.
4.   A.Agarwal (ed.):
Legal Control of Environmental Pollution
5.   Chetan Singh Mehta:
Environmental Protection and Law
6.   V.K. Krishna Iyyer:
Environment Pollution and Law
7.   Shah :
Environmental Law
8.   Paras Diwan :
Environmental Law and  Policy in India
,1991
9.   Dr. N. Maheshwara Swamy,
Environmental Law
, Asia Law House, Hyderabad




## LL.B. IV SEMESTER
### PAPER-I: LABOUR LAW-II
#### Unit-I:
The Remunerative Aspects – Wages – Concepts of wages - Minimum, Fair, Living Wages - Wage and
Industrial Policies - Whitley Commission Recommendations -Provisions of Payment of Wages Act 1936 - Timely
payment of wages - Authorized deductions – Claims - Minimum Wages Act 1948 - Definitions - Types of wages -
Minimum rates of wages - Procedure for fixing and revising Minimum Wages – Claims -Remedy.
#### Unit-II:
Bonus – concept - Right to claim Bonus – Full Bench formula - Bonus Commission - Payment of Bonus
Act 1965 - Application – Computation of gross profit, available, allocable surplus - Eligibility of Bonus -
Disqualification of Bonus - set on – set off of allocable surplus- Minimum and Maximum Bonus-Recovery of
Bonus.
#### Unit-III:
Employees Security and Welfare aspect - Social Security - Concept and meaning - Social Insurance -
Social Assistance Schemes. Social Security Legislations - Law relating to workmen’s compensation - The
Workmen’s Compensation Act 1923 – Definitions -Employer’s liability for compensation - Nexus between injury
and employment - payment of compensation - penalty for default - Employees State Insurance Act 1948 –
Application - Benefits under the Act - Adjudication of disputes and claims – ESI Corporation.
#### Unit-IV:
Employees Provident Fund and Miscellaneous Provisions Act 1952 – Contributions -Schemes under the
Act - Benefits. The Maternity Benefit Act 1961 - Definitions-Application - Benefits. The Payment of Gratuity Act
1972 – Definitions – application - Payment of gratuity - eligibility – forfeiture – Nomination - Controlling
authorities.
#### Unit-V:
The Factories Act 1948 - Chapters dealing with Health, Safety and Welfare of Labour. Child Labour -
Rights of child and the Indian Constitution - Salient features of the Child Labour (Prohibition and Regulation) Act
1986.
#### Suggested Readings
1. S.N.Misra, Labour and Industrial Laws, Central law publication-22nd edition. 2006.
2. N.G. Goswami, Labour and Industrial Laws, Central Law Agency.
3. Khan & Kahan, Labour Law-Asia Law house, Hyderabad
4. K.D. Srivastava, Payment of Bonus Act, Eastern Book Company
5. K.D. Srivastava, Payment of Wages Act
6. K.D. Srivastava, Industrial Employment (Standing Orders) Act 1947
7. S.C.Srivastava, Treatise on Social Security
8. Jidwitesukumar Singh, Labour Economics, Deep& Deep, New Delhi
9. V.J.Rao, Factories Law

### PAPER-II: PUBLIC INTERNATIONAL LAW
#### Unit-I:
Definition, Nature, Scope
and Importance of International Law — Relation of International Law to Municipal Law — Sources of International
Law — Codification.
#### Unit-II:
State Recognition — State Succession — Responsibility of States for International delinquencies —
State Territory — Modes of acquiring State Territory
#### Unit-III:
Position of Individual in International Law — Nationality — Extradition — Asylum — Privileges and
Immunities of Diplomatic Envoys — Treaties – Formation of Treaties - Modes of Consent, Reservation and
termination.
#### Unit-IV:
The Legal Regime of the Seas – Evolution of the Law of the Sea – Freedoms of the High Seas – Common
Heritage of Mankind – United Nations Convention on the Law of the Seas – Legal Regime of Airspace – Important
Conventions relating to Airspace – Paris, Havana, Warsaw and Chicago Conventions – Five Freedoms of Air –
Legal Regime of Outer space – Important Conventions such as Outer space Treaty, Agreement on Rescue and
Return of Astronauts, Liability Convention, Agreement on Registration of Space objects, Moon Treaty - Uni space.
#### Unit-V:
International Organizations — League of Nations and United Nations — International Court of Justice
—International Criminal Court - Specialized agencies of the UN — WHO, UNESCO, ILO, IMF and WTO.
#### Suggested Readings:
1. J.G. Starke: Introduction to International Law, Aditya Books, 10th Edition, 1989.
2. J.I. Brierly: The Law of Nations, Oxford Publishers, London.
3. Ian Brownlie: Principles of Public International Law, Oxford Publishers,
London.
4. S.K. Kapoor, Public International Law, Central Law Agencies, Allahabad.
5. H.O. Agarwal, International Law and Human Rights, Central Law Publications, Allahabad.
6 S.K. Verma, An Introduction to Public International Law, Prentice Hall of India.

### PAPER-III: interpretation of statutes
#### Unit-I:
Meaning and Definition of Statutes — Classification of Statues — Meaning and Definition of
Interpretation — General Principles of Interpretation — Rules of Construction under the General Clauses Act, 1897.
#### Unit-II:
Grammatical Rule of Interpretation — Golden Rule of Interpretation – Rule of Interpretation to avoid
mischief.
#### Unit-III:
Interpretation of Penal Statutes and Statutes of Taxation — Beneficial Construction — Construction to
avoid conflict with other provisions — Doctrine of Harmonious Construction.
#### Unit-IV:
External Aids to Interpretation — Statement of objects of legislation, Legislative debates, identification
of purpose sought to be achieved through legislation — Internal Aids to Interpretation — Preamble, title,
interpretation clause, marginal notes, explanations etc. — Presumptions.
#### Unit-V:
Effect of Repeal — Effect of amendments to statutes — Conflict between parent legislation and
subordinate legislation — Methods of interpreting substantive and procedural laws.
#### Suggested Readings:
1. Vepa P. Sarathi: Interpretation of Statutes, Eastern Book Co, 4th Edition, 1976.
2. Maxwell: Interpretation of Statutes, Butterworths Publications, 1976, 12th
Edition.
3. Crawford: Interpretation of Statutes, Universal Publishers.
4 Chatterjee: Interpretation of Statutes.
5. G.P. Singh: Principles of Statutory Interpretation, Wadhwa and Company, 8th Ed., 2001.
6. Cross, Statutory Interpretation

### PAPER-IV: land laws
#### Unit-I:
Classification of lands — Ownership of Land — Absolute and limited ownership (tenancy, lease etc.) —
Doctrine of Eminent Domain — Doctrine of Escheat - Doctrine of Bona Vacantia — Maintenance of land records
and issue of Pattas and Title Deeds etc.
#### Unit-II:
Law Reforms before and after independence — Zamindari Settlement — Ryotwari Settlement —
Mahalwari System — Intermediaries — Constitutional Provisions — Abolition of Zamindaries, Jagirs and Inams —
Tenancy Laws — Conferment of ownership on tenants/Ryots.
#### Unit-III:
Laws relating to acquisition of property — Land Acquisition Act of 1894 (Issue of notifications, Award
enquiry, Payment of compensation & Reference to civil courts etc.) The Land Acquisition and Requisition Act.
#### Unit-IV:
Laws relating to Ceiling on Land Holdings — A.P. Land Reforms (Ceiling on Agricultural Holdings)
Act, 1973 — Effect of inclusion in the IX Schedule of the Constitution — Interpretation of Directive Principles of
State Policy — The Urban Land (Ceiling on Holdings) Act, 1976.
#### Unit-V:
Laws relating to alienation — A.P. Scheduled Areas Land Transfer Regulation 1959 — A.P. Assigned
Lands (Prohibition of Transfers) Act, 1977-Resumption of Lands to the Transferor/Government - A.P. Land
Grabbing (Prohibition) Act.
#### Suggested Readings:
1. P. Rama Reddi and P. Srinivasa Reddy : Land Reform Laws in A.P., Asia Law House. 5th Ed. Hyderabad.
2. P.S. Narayana: Manual of Revenue Laws in A.P., Gogia Law Agency, 6th Ed. 1999, Hyderabad.
3. Land Grabbing Laws in A.P., Asia Law House, 3rd Ed. 2001, Hyderabad.
4. G.B. Reddy: Land Laws in A.P., Gogia Law Agency, Hyderabad, 1st Edition, 2001.

### PAPER-V:Intellectual property law
#### Unit-I:
Meaning, Nature, Classification and protection of Intellectual Property — The main forms of Intellectual
Property — Copyright, Trademarks, Patents, Designs (Industrial and Layout) -- Geographical Indications - Plant
Varieties Protection and Biotechnology.
#### Unit-II:
Introduction to the leading International instruments concerning Intellectual Property Rights — The
Berne Convention — Universal Copyright Convention — The Paris Union — Patent Co-operation Treaty -- The
World Intellectual Property Organization (WIPO) and the UNEESCO, International Trade Agreements concerning
IPR — WTO — TRIPS.
#### Unit-III:
Select aspects of the Law of Copyright in India — The Copy Right Act, 1957 - Historical evolution —
Meaning of copyright — Copyright in literary, dramatic and musical works, computer programmes and
cinematograph films — Neighboring rights — Rights of performers and broadcasters, etc. — Ownership and
Assignment of copyright — Author's special rights — Notion of infringement — Criteria of infringement —
Infringement of copyright in films, literary and dramatic works — Authorities under the Act — Remedies for
infringement of copyright.
#### Unit-IV:
Intellectual Property in Trademarks and the rationale of their protection - The Trade Marks Act, 1999 —
Definition of Trademarks — Distinction between Trademark and Property Mark - Registration — Passing off —
Infringement of Trademark — Criteria of Infringement — Remedies. The Designs Act, 2000 — Definition and
characteristics of Design — Law in India — Protection and rights of design holders — Copyright in design —
Registration — Remedies for infringement.
#### Unit-V:
Patents — Concept of Patent — Historical overview of the Patents Law in India — Patentable Inventions
— Kinds of Patents — Procedure for obtaining patent — The Patents Act, 1970 — Rights and obligations of a
patentee — Term of patent protection — Use and exercise of rights — Exclusive Marketing Rights — Right to
Secrecy — The notion of ‘abuse’ of patent rights — Infringement of patent rights and remedies available.
#### Suggested Readings:
1. P. Narayanan: Patent Law, Eastern Law House, 1995.
2. Roy Chowdhary, S.K. & Other: Law of Trademark, Copyrights, Patents and Designs,Kamal Law House, 1999.
3. Dr. G.B. Reddy, Intellectual Property Rights and the Law 5th Ed. 2005 GogiaLaw Agency.
4. John Holyoak and Paul Torremans: Intellectual Property Law.
5. B.L. Wadhera: Intellectual Property Law, Universal Publishers, 2nd Ed. 2000.
6. W.R. Cornish: Intellectual Property Law, Universal Publishers, 3rd Ed. 2001.
