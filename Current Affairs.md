


Chief Justice - Ranjan Gogoi
Telangana CJ - acting Chief Justice Raghavendra Singh Chauhan
Vikram Nath - new Chief Justice of A.P. High Court
Lokpal -  Pinaki Chandra Ghose 19th March
EC - Sunil Arora


NGT - Adarsh Kumar Goel


Rajeev Khel Ratna - Saikhom Mirabai Chanu, Virat Kohli

Bharat Ratna - Nanaji Deshmukh (Posthumous)
Bhupen Hazarika (Posthumous)
Pranab Mukherjee


IMF MD - Christine Lagarde

Chief Economist - Gita Gopinath




Supreme Court - 27 judges, sanctioned strength - 31


Common Cause v. Union of India - legalising passive euthanasia and living wills
 Govt. of NCT of Delhi v. Union of India & Anr - Lt Governors Powers



AAdhar case - Justice K.S. Puttaswamy


Abortion - Shah committee
The Medical Termination of Pregnancy Act, 1971 - 20 weeks limit



International Solar Alliance (ISA) - Headquarters - Gurugram
Head H.E. Upendra Tripathy
