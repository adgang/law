0. ICC Moot
http://iccmoot.com/
1. Philip C. Jessup International Law Moot Court Competition
https://www.ilsa.org/jessup-competitors/
2. Stetson International Environmental Moot Court Competition
https://www.stetson.edu/law/international/iemcc/
3. Price Media Law Moot Court Competition
http://pricemootcourt.socleg.ox.ac.uk/
4. Nelson Mandela World Human Rights Moot Court Competition
http://www.chr.up.ac.za/moot-courts
5. Henry Durant Human Rights Moot
http://www.isil-aca.org/moot_court_compt.htm
6. Red Cross International Humanitarian Law (IHL) Moot
http://web.redcross.org.hk/moot17/
7. Manfred Lachs Space Law Moot Court Competition
http://iislweb.org/awards-and-competitions/manfred-lachs-space-law-moot-court-competition/about-the-lachs-moot-competition/
8. The Annual Willem C. Vis International Commercial Arbitration Moot
https://vismoot.pace.edu/site/about-the-moot
9. Leiden-Sarin Air Law Moot Court Competition
https://www.legallyindia.com/wiki/Moot_court_competitions
10. Clara Bertan

11. Jean Pictet
http://www.concourspictet.org/ed2020en.html

India:
1. Nani



Moot  | Application
------|---------|-------------|

1. ICC Moot
National Round deadline: 10 November 2019
Application Deadline: Nov 24
- Last Date for Payment of Participation Fees: 15 February 2020
- Deadline for Memorial Submission: 28 February 2020
- Competition Dates: 27-29 March 2020

2. Jessup
Application Deadline: Nov 15
National Round: India
Contacts:	Vismay Shroff, Michael Peil
Dates: Jan 30-Feb 2, 2020
Location: Amity Law School
New Delhi
3. Stenson

India

Sponsor: Surana & Surana International Attorneys
Host: Jindal Global Law School, Sonipat, Haryana
Dates: February 14–16, 2020
Registration Deadline: November 14, 2019
Contact: Dr. S. Ravichandran (mootcourt@lawindia.com)
Registration and National Round Rules: www.moot.in
International Finals to be held in Gulfport, Florida, April 2-4, 2020

4. Price Media Law
Deadline passed
5. Nelson Mandela World Human Rights Moot Court Competition
Not elgigible
6. Henry Durant Human Rights Moot
http://www.isil-aca.org/moot_court_compt.htm
7. Red Cross International Humanitarian Law (IHL) Moot
http://web.redcross.org.hk/moot17/
8. Manfred Lachs Space Law Moot Court Competition
http://iislweb.org/awards-and-competitions/manfred-lachs-space-law-moot-court-competition/about-the-lachs-moot-competition/

https://iislweb.org/lachs_moot/
Still to be announced
9. The Annual Willem C. Vis International Commercial Arbitration Moot
https://vismoot.pace.edu/site/about-the-moot
Thursday, 28 November 2019	 Application Deadline
Saturday - Tuesday, 4 – 7 April 2020 Moot days
10. Leiden-Sarin Air Law Moot Court Competition
https://sarins.org/moot-court/
Nov 1
11. 2020 Clara Barton International Humanitarian Law Competition
Not eligible
12. Jean Pictet
February-March 2020, Denpasar (Indonesia)
The deadline for applications is 8 November 2019.
should be under 30 years of age.
http://www.concourspictet.org/ed2020en.html
